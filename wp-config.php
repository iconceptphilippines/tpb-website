<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tpbgovph_dbtpb2');

/** MySQL database username */
define('DB_USER', 'tpbgovph_admindb');

/** MySQL database password */
define('DB_PASSWORD', 'uFJ?Rk84#rf*');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'W(*{K:o2OnBM>B*3Y~yw=r$M0[FP!Zxw^YKaeEgC~!0Ez}~AF}~o$8a!,#EO-^6[');
define('SECURE_AUTH_KEY',  'XKQR+7]Ptw86Ar.SuPZgZeEvX-rlMPp4J6it)cd$n#<nOj.JvCJ%u_Wc.4y63C5.');
define('LOGGED_IN_KEY',    'en+LlK5#h/C((K(TC`_{/U4zA,S7}d4>5>2m);Iq~Fuw<dsat^DkpJR@6O{h)Hod');
define('NONCE_KEY',        'Am{Tz%l}h7}l7;^#]rIwAyUQ3el6:DqD|$[jnR*LLSXgxK;hwfvWC@sRiBU>-er3');
define('AUTH_SALT',        'py17l&2Y:WCF$Jw0CSPEl5S1fpU7,u!4l[:C#whv3VW&q(d(W{6Wm?0_:xgzS@@.');
define('SECURE_AUTH_SALT', 'W|q4h%&EsP*R2,_d[9e@y!S7,c_y[#=CPoq]*,;Z7?Itvb2t{3iPUjL:o;Qe:2^k');
define('LOGGED_IN_SALT',   'm=AHfygX+d`m9MD[E*1fD7i$:2PQ?^*4HI$w)#C24Emg.JZ#!q@3,T/B5gU1;M_{');
define('NONCE_SALT',       '61x+!(H5On@__{>yZN|Sl`UP$btR.5ek,L UT/ AUD|VpA-axA+KzX,350?mvjU:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tpb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/* Updates */
define( 'WP_AUTO_UPDATE_CORE', false );
define( 'DISALLOW_FILE_MODS', true );
define( 'DISALLOW_FILE_EDIT', true );
