<div id="nested<?php echo $postid2; ?>" class="panel-collapse collapse <?php echo $class2; ?> body2">
	<div class="panel-body">
		<div class="panel-group" id="postnested<?php echo $postid2; ?>">
			<?php
			$years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'csr' ORDER BY post_date DESC");
			if( $years ):
				foreach( $years as $year ):
					if( isset($_GET['y']) ):
						if( $_GET['y'] == $year && $currentid == $post->ID ):
							$classyear = 'activeyear';
							$classcollapse = 'in';
						else:
							$classyear = '';
							$classcollapse = '';							
						endif;
					else:
						$classyear = '';
						$classcollapse = '';
					endif;
					?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a class="title4 raleway easeme year <?php echo $classyear; ?>" href="<?php echo get_the_permalink().'?y='.$year; ?>">
								<?php echo $year; ?>
							</a>
						</div><!--/.panel-heading -->
					</div>
				<?php endforeach;
			endif; ?>
		</div> <!-- end of panel-group -->
	</div> <!-- end of panel-body -->
</div> <!-- end of panel-collapse -->