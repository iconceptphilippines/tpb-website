<div id="nested<?php echo $postid2; ?>" class="panel-collapse collapse <?php echo $class2; ?> body2">
	<div class="panel-body">
		<div class="panel-group" id="postnested<?php echo $postid2; ?>">
			<?php
			$years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'csr' ORDER BY post_date DESC");
			if( $years ):
				foreach( $years as $year ):
					if( isset($_GET['y']) ):
						if( $_GET['y'] == $year && $currentid == $post->ID ):
							$classyear = 'activeyear';
							$classcollapse = 'in';
						else:
							$classyear = '';
							$classcollapse = '';							
						endif;
					else:
						$classyear = '';
						$classcollapse = '';
					endif;
					?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" class="title4 raleway easeme year <?php echo $classyear; ?>" data-parent="#nested<?php echo $slug; ?>" href="#nested-<?php echo $slug.$year ?>">
								<?php echo $year; ?>
							</a>
						</div><!--/.panel-heading -->
						<div id="nested-<?php echo $slug.$year ?>" class="panel-collapse collapse <?php echo $classcollapse; ?>">
							<div class="panel-body">
								<?php $months = $wpdb->get_col("SELECT DISTINCT MONTH(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'csr' AND YEAR(post_date) = '".$year."' ORDER BY post_date ASC");
								foreach( $months as $month ):
									if( isset($_GET['month']) ):
										if( $_GET['month'] == $year  ):
											$classmonth = 'activemonth';
										else:
											$classmonth = '';
										endif;
									else:
										$classmonth = '';
									endif;

									$monthletter = date( 'F', mktime(0, 0, 0, $month,1 ) ); ?>
									<a class="title5 year raleway easeme month <?php echo $classmonth; ?>" href="<?php echo get_the_permalink().'?y='.$year.'&month='.$month ?>">
										<?php echo $monthletter; ?>
									</a>
								<?php endforeach; ?>

							</div>
						</div>
					</div>
				<?php endforeach;
			endif; ?>
		</div> <!-- end of panel-group -->
	</div> <!-- end of panel-body -->
</div> <!-- end of panel-collapse -->