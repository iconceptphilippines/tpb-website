<?php
if( isset( $_GET['y'] ) ):
	$year = $_GET['y'];
else:
	$year = '';
endif;
if( isset( $_GET['month'] ) ):
	$month = $_GET['month'];
else:
	$month = '';
endif;
if( isset( $_GET['orderby'] ) ):
	$orderby = $_GET['orderby'];
else:
	$orderby = 'name';
endif;
if( isset( $_GET['id'] ) ):
	$post_id_name = $_GET['id'];
else:
	$post_id_name = '';
endif;
if( get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ):
	$posttype = 'mice-national';
	$slug = 'ntl';
elseif( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID ):
	$posttype = 'mice-international';
else:
	$posttype = '';
endif;
?>

<div class="formmice">
	<div class="row">
		<div class="col-sm-3">
			<div class="right">
				<?php if( $orderby == 'name' || $orderby == '' ):
					$checked = 'checked';
				else:
					$checked = '';
				endif; ?>
				<label for="event" class="labelfor raleway">Filter by Event:</label>								
			    <div class="roundedOne">
			    <input type="radio" value="<?php echo get_the_permalink().'?y='.$year.'&month='.$month.'&orderby=name'; ?>" id="event" name="filter" class="easeme" <?php echo $checked; ?> />
			    <label for="event"></label>
			    </div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="right">
				<?php if( $orderby == 'date' ):
					$checked = 'checked';
				else:
					$checked = '';
				endif; ?>
				<label for="date" class="labelfor raleway">Filter by Date:</label>
			    <div class="roundedOne">
			      <input type="radio" value="<?php echo get_the_permalink().'?y='.$year.'&month='.$month.'&orderby=date'; ?>" id="date" name="filter" class="easeme" <?php echo $checked; ?> />
			      <label for="date"></label>
			    </div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="right">
				<?php
				$thislink = get_the_permalink();
				$args = array(
					'post_type' => $posttype,
					'orderby'	=> 'name',
					'order'		=> 'ASC',
					'posts_per_page' => -1 );
				$variable = new WP_Query($args);
				if ($variable->have_posts()): ?>
					<select name="postid" id="postid" class="form-control raleway"> <option value="">-Select Event-</option>
						<?php while( $variable->have_posts() ): $variable->the_post();
							if( $post_id_name == $post->ID ):
								$select = 'selected';
							else:
								$select = '';
							endif;
							?>
							<option value="<?php echo $thislink.'?id='.get_the_id(); ?>" <?php echo $select; ?> ><?php the_title(); ?></option>
						<?php endwhile; wp_reset_postdata(); ?>
					</select>
				<?php endif; ?>								
			</div>
		</div>
	</div>
	
</div> <!-- end of formmice -->

<script type="text/javascript">
	jQuery(window).load(function($){
	    jQuery('input[name="filter"]').change(function() {
	       var val = jQuery(this).val();
	       window.location = val;
	    });
	    jQuery('select[name="postid"]').change(function() {
	       var val = jQuery(this).val();
	       window.location = val;
	    });
	});
</script>