<?php
require('../mpdf/mpdf.php');
require('../../../../wp-load.php');
global $wpdb;
$mpdf = new mPDF('','', 0, '', 0, 0, 0, 0);

if( !isset( $_GET['type'] ) ):
	return false;
endif;
$the_query = array(
    'post_type'  => 'events',  
	'tax_query' => array(
		array(
			'taxonomy' => 'type',
            'field' => 'id',
            'terms' => $_GET['type'],
            'operator' => 'IN'
		),
	),    
	'posts_per_page' => -1 );
$variable = new WP_Query($the_query);
if ( $variable->have_posts() ): $country_array = array();
	while ($variable->have_posts()): $variable->the_post();
		if( !in_array(  get_field( 'country' ), $country_array ) ):
			$country_array[] = get_field( 'country' );
		endif;
	endwhile; wp_reset_postdata();
	sort( $country_array );
endif;

$type = 0;


if( isset( $_GET['country'] ) ):
	$type = 1;
	$the_query2 = array(
	    'post_type'  => 'events',  
		'order'	=> 'ASC',
		'orderby'	=> 'meta_value',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'	=> 'country',
				'value'=> $_GET['country'],
				'type' => 'CHAR',
				'compare' => '=', 
			),
		),
		'tax_query' => array(
			array(
				'taxonomy' => 'type',
	            'field' => 'id',
	            'terms' => $_GET['type'],
	            'operator' => 'IN'
			),
		),
		'posts_per_page' => -1 );
elseif( isset( $_GET['postdate'] ) ):
	$type = 2;
	$the_query2 = array(
	    'post_type'  => 'events',  
		'order'	=> $_GET['postdate'],
		'tax_query' => array(
			array(
				'taxonomy' => 'type',
	            'field' => 'id',
	            'terms' => $_GET['type'],
	            'operator' => 'IN'
			),
		),
		'orderby'	=> 'date',
		'posts_per_page' => -1 );
elseif( isset( $_GET['postevent'] ) ):
	$type = 2;
	$the_query2 = array(
		'page_id' => $_GET['postevent'],
		'tax_query' => array(
			array(
				'taxonomy' => 'type',
	            'field' => 'id',
	            'terms' => $_GET['type'],
	            'operator' => 'IN'
			),
		),
	    'post_type'  => 'events',  
		'order'	=> 'DESC',
		'orderby'	=> 'date',
		'posts_per_page' => -1 );
endif;

$html = '<html lang="en">';
$html .= '<head>';
	$html .= '<meta charset="UTF-8">';
	$html .= '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/bootstrap.css">';
	$html .= '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/dev1.css">';
$html .= '</head>';
$html .= '<body>';
$html .= '<div class="container" id="page">';
	$html .= '<div class="row" class="mice-cont">';
		$html .= '<div class="mpdf">';
			$html .= '<div class="col-xs-12">';
				$html .= '<div class="row">';
					$html .= '<div class="col-xs-12">';
						$html .= '<img src="'.get_bloginfo('template_url').'/images/mainlogo.png" alt="icon" style="margin:auto; text-align:center">';
					$html .= '<div>';
				$html .= '<div>';
				$html .= '<table class="table table-responsive" id="eventlists">';
					$html .= '<thead>';
						$html .= '<tr>';
							$html .= '<th width="20%">NAME OF EVENT</th>';
							$html .= '<th width="15%">DATE/VENUE</th>';
							$html .= '<th width="40%">BRIEF DESCRIPTION</th>';
							$html .= '<th width="25%">PROJECT OFFICER</th>';
						$html .= '</tr>';
					$html .= '</thead>';
					$html .= '<tbody>';
						if( $type == 1 ): 
							$name = str_replace('_', ' ', $_GET['country'] );
							$html .= '<tr class="e-country"><td colspan="4" class="raleway"><span>'.strtoupper( $name ).'</span></td></tr>';
							$variable2 = new WP_Query($the_query2);
							if ( $variable2->have_posts() ):				
								while ($variable2->have_posts()): $variable2->the_post();
									$html .= '<tr>';
										$html .= '<td width="20%" class="e-name raleway">'.get_the_title().'</td>';
										$html .= '<td width="15%" class="e-venue-date raleway">';
											if( get_field('date') ): 
												$html .= get_field('date');
											endif;
											if( get_field('venue') ): 
												$html .= get_field('venue');
											endif;	
										$html .= '</td>';
										$html .= '<td width="40%" class="e-desc raleway">'.get_the_content().'</td>';
										$html .= '<td width="25%" class="e-proj-off raleway">';
											if( get_field('project_officer') ):
												$html .= get_field('project_officer'); 
											endif;
										$html .= '</td>';
									$html .= '</tr>';
								endwhile;	
							else:
								$html .= '<tr><td colspan="4" class="raleway center">No result found.</td></tr>';
							endif; wp_reset_postdata();
						elseif( $type == 2 ):
							$variable2 = new WP_Query($the_query2);
							if ( $variable2->have_posts() ):				
								while ($variable2->have_posts()): $variable2->the_post();
									$name = str_replace('_', ' ', get_field('country') );									
									$html .= '<tr class="e-country"><td colspan="4" class="raleway"><span>'.strtoupper( $name ).'</span></td></tr>';
									$html .= '<tr>';
										$html .= '<td width="20%" class="e-name raleway">'.get_the_title().'</td>';
										$html .= '<td width="15%" class="e-venue-date raleway">';
											if( get_field('date') ): 
												$html .= get_field('date');
											endif;
											if( get_field('venue') ): 
												$html .= get_field('venue');
											endif;	
										$html .= '</td>';
										$html .= '<td width="40%" class="e-desc raleway">'.get_the_content().'</td>';
										$html .= '<td width="25%" class="e-proj-off raleway">';
											if( get_field('project_officer') ):
												$html .= get_field('project_officer'); 
											endif;
										$html .= '</td>';
									$html .= '</tr>';
								endwhile;
							else:
								$html .= '<tr><td colspan="4" class="raleway center">No result found.</td></tr>';
							endif; wp_reset_postdata();	
						else:
							foreach( $country_array as $country ):
								$name = str_replace('_', ' ', $country );								
								$html .= '<tr class="e-country"><td colspan="4" class="raleway"><span>'.strtoupper( $name ).'</span></td></tr>';
								$the_query2 = array(
								    'post_type'  => 'events',  
									'order'	=> 'ASC',
									'orderby'	=> 'meta_value',
									'meta_query' => array(
										'relation' => 'AND',
										array(
											'key'	=> 'country',
											'value'=> $country,
											'type' => 'CHAR',
											'compare' => '=', 
										),
									),
									'tax_query' => array(
										array(
											'taxonomy' => 'type',
								            'field' => 'id',
								            'terms' => $_GET['type'],
								            'operator' => 'IN'
										),
									),
									'posts_per_page' => -1 );
								$variable2 = new WP_Query($the_query2);
								if ( $variable2->have_posts() ):				
									while ($variable2->have_posts()): $variable2->the_post();

										$html .= '<tr>';
											$html .= '<td width="20%" class="e-name raleway">'.get_the_title().'</td>';
											$html .= '<td width="15%" class="e-venue-date raleway">';
												if( get_field('date') ): 
													$html .= get_field('date');
												endif;
												if( get_field('venue') ): 
													$html .= get_field('venue');
												endif;	
											$html .= '</td>';
											$html .= '<td width="40%" class="e-desc raleway">'.get_the_content().'</td>';
											$html .= '<td width="25%" class="e-proj-off raleway">';
												if( get_field('project_officer') ):
													$html .= get_field('project_officer'); 
												endif;
											$html .= '</td>';
										$html .= '</tr>';

									endwhile;
								else:
									$html .= '<tr><td colspan="4" class="raleway center">No result found.</td></tr>';
								endif;
							endforeach;
						endif;
					$html .= '</tbody>';
				$html .= '</table>'; //end of table
			$html .= '</div>'; // end of col-xs-12
		$html .= '</div>'; // end of mpdf
	$html .= '</div>'; //end of row
$html .= '</div>'; //end of container
$html .= '</body>';
$html .= '</html>';

$file_name = 'tpb-mice.pdf';

// $mpdf->Bookmark();
$mpdf->WriteHTML($html);
$mpdf->Output($file_name, 'D');

exit;
?>