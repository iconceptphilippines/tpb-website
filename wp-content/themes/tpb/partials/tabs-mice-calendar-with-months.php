<?php
if( get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ):
	$posttype = 'mice-national';
	$slug = 'ntl';
elseif( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID ):
	$posttype = 'mice-international';
	$slug = 'int';
else:
	$posttype = '';
	$slug = '';	
endif;
if( $currentid == $post->ID ):
	$class = 'active3';
	$class2 = 'in';
else:
	$class = '';
	$class2 = '';
endif;
?>
<div id="nested<?php echo $post->ID.'-'.$sctr; ?>" class="panel-collapse collapse <?php echo $class2; ?> body3">
	<div class="panel-body">
		<div class="panel-group" id="nested<?php echo $slug; ?>">
			<?php
			$years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".$posttype."' ORDER BY post_date DESC");
			if( $years ):
				foreach( $years as $year ):
					if( isset($_GET['y']) ):
						if( $_GET['y'] == $year && $currentid == $post->ID ):
							$classyear = 'activeyear';
							$classcollapse = 'in';
						else:
							$classyear = '';
							$classcollapse = '';							
						endif;
					else:
						$classyear = '';
						$classcollapse = '';
					endif;
					?>
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" class="title4 raleway easeme year <?php echo $classyear; ?>" data-parent="#nested<?php echo $slug; ?>" href="#nested-<?php echo $slug.$year ?>">
								<?php echo $year; ?>
							</a>
						</div><!--/.panel-heading -->
						<div id="nested-<?php echo $slug.$year ?>" class="panel-collapse collapse <?php echo $classcollapse; ?>">
							<div class="panel-body">
								<?php $months = $wpdb->get_col("SELECT DISTINCT MONTH(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".$posttype."' AND YEAR(post_date) = '".$year."' ORDER BY post_date ASC");
								foreach( $months as $month ):
									if( isset($_GET['month']) ):
										if( $_GET['month'] == $year  ):
											$classmonth = 'activemonth';
										else:
											$classmonth = '';
										endif;
									else:
										$classmonth = '';
									endif;

									$monthletter = date( 'F', mktime(0, 0, 0, $month,1 ) ); ?>
									<a class="title5 year raleway easeme month <?php echo $classmonth; ?>" href="<?php echo get_the_permalink().'?y='.$year.'&month='.$month ?>">
										<?php echo $monthletter; ?>
									</a>
								<?php endforeach; ?>
							</div><!--/.panel-body -->
						</div><!--/.panel-collapse --> 
					</div><!-- /.panel --> 
					
				<?php endforeach;
			endif; ?>
		</div> <!-- end of year -->
	</div> <!-- end of panel-body -->
</div>