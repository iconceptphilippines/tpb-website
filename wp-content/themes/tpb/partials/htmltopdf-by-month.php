<?php
require('../mpdf/mpdf.php');
require('../../../../wp-load.php');
global $wpdb;
$mpdf = new mPDF('','', 0, '', 0, 0, 0, 0);

if( !isset( $_GET['type'] ) ):
	return false;
endif;




$type = 0;


if( isset( $_GET['postevent'] ) ):
	$type = 1;
	$query = array(
		'page_id' => $_GET['postevent'],
		'tax_query' => array(
			array(
				'taxonomy' => 'type',
	            'field' => 'id',
	            'terms' => $_GET['type'],
	            'operator' => 'IN'
			),
		),
	    'post_type'  => 'events',  
		'order'	=> 'DESC',
		'orderby'	=> 'date',
		'posts_per_page' => -1 );

	$event_query = new WP_Query($query);
	if ( !$event_query->have_posts() ):
		return false;
	endif;

elseif( isset( $_GET['mnth'] ) ):
	$type = 2;
	$query = array(
		'post_type'  => 'events', 
		'tax_query' => array(
			array(
				'taxonomy' => 'type',
		        'field' => 'id',
		        'terms' => $_GET['type'] ,
		        'operator' => 'IN'
			),
		),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'month_filter',
				'value'	=> $_GET['mnth'],
				'compare' => '=',
				'type' => 'CHAR',  
			),
		),
		'orderby'	=> 'menu_order',
		'order'		=> ASC,
		'posts_per_page' => -1 );

	$month_query = new WP_Query($query);

	if ( !$month_query->have_posts() ):
		return false;
	endif;
else:
	$type = 3;
endif;


$html = '<html lang="en">';
$html .= '<head>';
	$html .= '<meta charset="UTF-8">';
	$html .= '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/bootstrap.css">';
	$html .= '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/dev1.css">';
$html .= '</head>';
$html .= '<body>';
$html .= '<div class="container" id="page">';
	$html .= '<div class="row" class="mice-cont">';
		$html .= '<div class="mpdf">';
			$html .= '<div class="col-xs-12">';
				$html .= '<div class="row">';
					$html .= '<div class="col-xs-12">';
						$html .= '<img src="'.get_bloginfo('template_url').'/images/mainlogo.png" alt="icon" style="margin:auto; text-align:center">';
					$html .= '<div>';
				$html .= '<div>';
				$html .= '<table class="table table-responsive" id="eventlists">';
					$html .= '<thead>';
						$html .= '<tr>';
							$html .= '<th width="25%">NAME OF EVENT</th>';
							$html .= '<th width="25%">DATE/VENUE</th>';
							$html .= '<th width="50%">BRIEF DESCRIPTION</th>';
						$html .= '</tr>';
					$html .= '</thead>';
					$html .= '<tbody>';
						if( $type == 1 ):

							if ( $event_query->have_posts() ): $month_ctr = 1;
								while( $event_query->have_posts() ): $event_query->the_post();
									if( $month_ctr == 1 ):
										$dateObj   = DateTime::createFromFormat('!m', get_field('month_filter') );
										$monthName = $dateObj->format('F');
										if( get_field( get_field( 'month_filter' ).'_description', 'type_'.$_GET['type'] ) ):
											$monthdescp = get_field( get_field( 'month_filter' ).'_description', 'type_'.$_GET['type'] );
										else:
											$monthdescp = '';
										endif;
										if( get_field( get_field( 'month_filter' ).'_event', 'type_'.$_GET['type'] ) ):
											$monthcont = get_field( get_field( 'month_filter' ).'_event', 'type_'.$_GET['type'] );
										else:
											$monthcont = '';
										endif;
										$html .= '<tr class="e-country">';
											$html .= '<td colspan="1" class="raleway">'.$monthName.' - '.$monthdescp.'</td>';
											$html .= '<td colspan="3">'.$monthcont.'</td>';
										$html .= '</tr>';
									endif; $month_ctr++; 
									$html .= '<tr>';
										$html .= '<td width="25%" class="e-name raleway" id="'.get_the_id().'">'.get_the_title().'</td>';
										$html .= '<td width="25%" class="e-venue-date raleway">';
											if( get_field('date') ): 
												$html .= get_field('date');
											endif;
											if( get_field('venue') ): 
												$html .= get_field('venue');
											endif;												
										$html .= '</td>';
										$html .= '<td width="50%" class="e-desc raleway">'.get_the_content().'</td>';
									$html .= '</tr>';
								endwhile;
								wp_reset_postdata();
							endif;
						elseif( $type == 2 ):
							$dateObj   = DateTime::createFromFormat('!m', $_GET['mnth'] );
							$monthName = $dateObj->format('F');
							if( get_field( $_GET['mnth'].'_description', 'type_'.$_GET['type'] ) ):
								$monthdescp = get_field( $_GET['mnth'].'_description', 'type_'.$_GET['type'] );
							else:
								$monthdescp = '';
							endif;
							if( get_field( $_GET['mnth'].'_event', 'type_'.$_GET['type'] ) ):
								$monthcont = get_field( $_GET['mnth'].'_event', 'type_'.$_GET['type'] );
							else:
								$monthcont = '';
							endif;
							$html .= '<tr class="e-country">';
								$html .= '<td colspan="1" class="raleway">'.$monthName.' - '.$monthdescp.'</td>';
								$html .= '<td colspan="3">'.$monthcont.'</td>';
							$html .= '</tr>';

							if ( $month_query->have_posts() ):
								while ($month_query->have_posts()): $month_query->the_post();
									$html .= '<tr>';
										$html .= '<td width="25%" class="e-name raleway" id="'.get_the_id().'">'.get_the_title().'</td>';
										$html .= '<td width="25%" class="e-venue-date raleway">';
											if( get_field('date') ): 
												$html .= get_field('date');
											endif;
											if( get_field('venue') ): 
												$html .= get_field('venue');
											endif;												
										$html .= '</td>';
										$html .= '<td width="50%" class="e-desc raleway">'.get_the_content().'</td>';
									$html .= '</tr>';
								endwhile;
								wp_reset_postdata();
							endif;
						else:
							$thistype = $_GET['type'];
							$month_filter = $wpdb->get_col('SELECT DISTINCT(mt1.meta_value) 
							               FROM '.$wpdb->posts.' AS p1 
							               INNER JOIN '.$wpdb->postmeta.' AS mt1 ON p1.ID = mt1.post_id
							               INNER JOIN '.$wpdb->postmeta.' AS mt2 ON p1.ID = mt2.post_id
							               INNER JOIN '.$wpdb->term_relationships.' AS t1 ON p1.ID = t1.object_id
							   
							               AND mt1.meta_key = "month_filter"
							               AND t1.term_taxonomy_id = '.$thistype.'
							               AND p1.post_type = "events"
							               AND p1.post_status = "publish"
							               WHERE mt1.meta_value != ""
							               ORDER BY mt1.meta_value ASC');
							sort($month_filter);
							if( $month_filter ):
								foreach( $month_filter as $month ):
									$dateObj   = DateTime::createFromFormat('!m', $month);
									$monthName = $dateObj->format('F');
									$query = array(
									    'post_type'  => 'events', 
										'tax_query' => array(
											array(
												'taxonomy' => 'type',
								                'field' => 'id',
								                'terms' => $thistype,
								                'operator' => 'IN'
											),
										),
										'meta_query' => array(
											'relation' => 'AND',
											array(
												'key' => 'month_filter',
												'value'	=> $month,
												'compare' => '=',
												'type' => 'CHAR',  
											),
										),
										'orderby'	=> 'menu_order',
										'order'		=> ASC,
										'posts_per_page' => -1 );
									$var = new WP_Query($query);
									if( get_field( $month.'_description', 'type_'.$_GET['type'] ) ):
										$monthdescp = get_field( $month.'_description', 'type_'.$_GET['type'] );
									else:
										$monthdescp = '';
									endif;
									if( get_field( $month.'_event', 'type_'.$_GET['type'] ) ):
										$monthcont = get_field( $month.'_event', 'type_'.$_GET['type'] );
									else:
										$monthcont = '';
									endif;
									$html .= '<tr class="e-country">';
										$html .= '<td colspan="1" class="raleway">'.$monthName.' - '.$monthdescp.'</td>';
										$html .= '<td colspan="3">'.$monthcont.'</td>';
									$html .= '</tr>';

									if ( $var->have_posts() ):
										while ($var->have_posts()): $var->the_post(); 
											$html .= '<tr>';
												$html .= '<td width="25%" class="e-name raleway" id="'.get_the_id().'">'.get_the_title().'</td>';
												$html .= '<td width="25%" class="e-venue-date raleway">';
													if( get_field('date') ): 
														$html .= get_field('date');
													endif;
													if( get_field('venue') ): 
														$html .= get_field('venue');
													endif;												
												$html .= '</td>';
												$html .= '<td width="50%" class="e-desc raleway">'.get_the_content().'</td>';
											$html .= '</tr>';
										endwhile;
									endif;

								endforeach;
							endif;
						endif;
					$html .= '</tbody>';
				$html .= '</table>'; //end of table
			$html .= '</div>'; // end of col-xs-12
		$html .= '</div>'; // end of mpdf
	$html .= '</div>'; //end of row
$html .= '</div>'; //end of container
$html .= '</body>';
$html .= '</html>';

$file_name = 'tpb-mice.pdf';

// $mpdf->Bookmark();
$mpdf->WriteHTML($html);
$mpdf->Output($file_name, 'D');

exit;
?>