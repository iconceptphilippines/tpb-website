<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	 	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			<?php 
				wp_title( '|', true, 'right' ); 
			?>
		</title>

		<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png"  type="image/png"/>
		<?php wp_head(); ?>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-85803189-1', 'auto');
		  ga('send', 'pageview');

		</script>
</head>	
<body <?php body_class(); ?> >

<header>
	<div class="menucont">	
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-2 col-xs-2 col-main1">
					<div class="mainlogo">
						<?php if( get_field( 'mainlogo', 'options' ) ):
							$main_logo = get_field( 'mainlogo', 'options' );
						else:
							$main_logo = get_bloginfo('template_url').'/images/mainlogo.png';
						endif; ?>
						<a href="<?php echo site_url(); ?>"><img src="<?php echo $main_logo ?>" alt="tpb logo" class="img-responsive img-center"></a>
					</div>
				</div>
				<div class="col-md-9 col-sm-10 col-xs-10 col-main2">
					<div class="container-fluid nopadding">
						<div class="row">
							<div class="col-sm-12">
								<?php if( get_field( 'members','options' ) ): ?>
									<p class="right pull-right mem-lists">Member of:
									<?php
									$members = get_field( 'members','options' );
									foreach( $members as $member ):
										if( $member['link'] ): ?>
											<a href="<?php echo $member['link']; ?>" target="_blank"><img src="<?php echo $member['member_logo']; ?>" alt="member"></a>
										<?php else: ?>
											<img src="<?php echo $member['member_logo']; ?>" alt="member">
										<?php endif; ?>
								<?php endforeach;
								endif; ?>
								</p>
							</div>
						</div>
					</div>
				</div> <!-- end of col-lg-9 -->
				<div class="col-lg-9 col-md-12 col-sm-12">
					<div class="mainmenu pull-right right">
						<ul>							
							<?php
							$menuargs = array(
							'theme_location'    =>  'main-menu',
							'container'         =>  '',
							'menu_class'        =>  '',
							'menu_id'           =>  '',
							'items_wrap'      => '%3$s',
							);
							wp_nav_menu($menuargs); ?>					
						</ul>
					</div>
					<div class="clearfix"></div>
					<div class="pull-right">
						<a href="#headermenu2" class="sh-button">
							<span class="sm-btn"><i class="fa fa-bars"></i></span>
						</a>
					</div>
					<div id="headermenu2" class="pull-right">
						<ul>
							<?php
								$menuargs = array(
								'theme_location'    =>  'main-menu',
								'container'         =>  '',
								'menu_class'        =>  '',
								'menu_id'           =>  '',
								'items_wrap'      => '%3$s',
								);
								wp_nav_menu($menuargs); ?>
						</ul>								
					</div> <!-- end of headermenu1 -->
				</div>
			</div> <!-- end of row -->
		</div> <!-- end of container-->
	</div>  <!-- end of menucont-->
	<?php if( is_home() ): ?>
		<div id="banner" style='height:528px; background:#ccc'>
			<?php if( get_field( 'banner_shortcode', 'options' ) ):
				$id = get_field( 'banner_shortcode', 'options' );
				echo do_shortcode("[layerslider id=".$id."]");
			endif; ?>
		</div>
	<?php else:
		$image = wp_get_attachment_url( get_post_thumbnail_id() );
		if(!$image):
			$image = get_bloginfo('template_url').'/images/image_banner_fix.png';
		endif;
		if( is_post_type_archive('invitation-to-bid') ):
			if( get_field( 'invitation_to_bid_banner_image','options') ):
				$image = get_field( 'invitation_to_bid_banner_image','options');
			endif;		
		elseif( is_singular('invitation-to-bid') ):
			if( get_field('banner_image') ):
				$image = get_field('banner_image');
			else:
				if( get_field( 'invitation_to_bid_banner_image','options') ):
					$image = get_field( 'invitation_to_bid_banner_image','options');
				endif;
			endif;
		elseif( is_post_type_archive('whats-new') ):
			if( get_field( 'whats_new_banner_image','options') ):
				$image = get_field( 'whats_new_banner_image','options');
			endif;
		elseif( is_singular('whats-new') ):
			if( get_field('banner_image') ):
				$image = get_field('banner_image');
			else:
				if( get_field( 'whats_new_banner_image','options') ):
					$image = get_field( 'whats_new_banner_image','options');
				endif;
			endif;
		elseif( is_singular( 'mice-international' ) ):
			if( get_field( 'internation_banner_image','options') ):
				$image = get_field( 'internation_banner_image','options');
			endif;
		elseif( is_singular( 'mice-national' ) ):
			if( get_field( 'national_banner_image','options') ):
				$image = get_field( 'national_banner_image','options');
			endif;
		elseif( is_singular( 'alpha-listing' ) ):
			if( get_field( 'alpha_banner_image','options') ):
				$image = get_field( 'alpha_banner_image','options');
			endif;
		elseif( is_404() ):
			if( has_post_thumbnail( get_id_using_template('templates/template-not-found.php') ) ):
				$image = wp_get_attachment_url( get_post_thumbnail_id( get_id_using_template('templates/template-not-found.php') )) ;				
			endif;

		endif;

		global $post;
		if( is_post_type_archive('whats-new') ):
			$title = "What's New";
		elseif( is_post_type_archive('invitation-to-bid') ):
			$title = "Invitation to Bid";
		elseif( is_404() ):
			$title = get_the_title( get_id_using_template('templates/template-not-found.php') );
		elseif( $post->post_parent ):
			$title = get_the_title( $post->post_parent );
		else:
			$title = get_the_title();
		endif;
		?>
<!-- 		<div id="banner-image" style="background: url(<?php //echo $image; ?>) center 0px no-repeat; background-size:cover; -o-background-size: cover; -moz-background-size: cover; -webkit-background-size: cover; height: 144px; ">			
		</div> -->
		<div id="banner-image">
			<img src="<?php echo $image; ?>" alt="<?php the_title(); ?>" class="imgcenter img-responsive fullwidth">
		</div>
		<div class="container">
			<div class="row">
				<h1 class="page-title raleway"><img src="<?php echo get_bloginfo('template_url').'/images/mini_title_icon.png'; ?>" alt=""> <?php echo $title; ?></h1>
			</div>
		</div>
	<?php endif; ?>
</header>