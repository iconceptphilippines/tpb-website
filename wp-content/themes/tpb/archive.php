<?php get_header(); ?>

<div id="archive">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php echo get_sidebar(); ?>
			</div>
			<div class="col-md-9">
				<div class="row">
					<?php if(have_posts()): while( have_posts() ): the_post();
						if( has_post_thumbnail() ):
							$image = wp_get_attachment_url( get_post_thumbnail_id() );
						else:
							$image = get_bloginfo('template_url')."/images/dummyimg2.png";
						endif; ?>
						<div class="col-md-4 col-sm-6">
							<div class="glbl-cont">
								<div class="glbl-img" style="background: url(<?php echo $image; ?>) center 0px no-repeat; background-size:cover; -o-background-size: cover; -moz-background-size: cover; -webkit-background-size: cover;"></div>
								<p class="glbl-title"><?php the_title(); ?></p>
								<p class="glbl-excerpt">
									<?php
									$myExcerpt = get_the_excerpt();
									$tags = array("<p>", "</p>");
									$myExcerpt = str_replace($tags, "", $myExcerpt);
									$strcount = strlen($myExcerpt);
									$newexcerpt = substr($myExcerpt, 0,118);
									if ($strcount <= 120):
										echo $myExcerpt;
									else:
										echo $newexcerpt.'...';
									endif;
									?>									
								</p>
								<div class="glbl-pulldown">
									<p class="pull-left glbl-time"><?php the_time('F d, Y') ?></p><p class="glbl-link pull-right"><a class="btn glbl-btn1 easeme" href="<?php the_permalink(); ?>">READ MORE</a></p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					<?php endwhile; wp_reset_postdata();
					endif; ?>			
					<div class="col-xs-12">
						<div class="pagination fullwidth">
							<div class="right">
								<?php echo tpb_archive_pagination(); ?>									
							</div>
						</div>
					</div>
				</div>
			</div> <!-- end of col-lg-9 -->

		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<script type="text/javascript">
	jQuery(window).load(function($){
    	var biggestHeight = 0;

	    jQuery('.glbl-cont').each( function(){
		    if( jQuery(this).height() > biggestHeight ){
	    		biggestHeight = jQuery(this).height() + 20;;
	    	}
	    });
		jQuery('.glbl-cont').height( biggestHeight );


	});
</script>

<?php get_footer(); ?>