<div id="<?php echo get_post_type() ?>-archive" class="panel-collapse sidebar-archive">
	<div class="panel-body">
		<div class="panel-group" id="sidebar-<?php echo get_post_type(); ?>">
			<?php
			$years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".get_post_type()."' ORDER BY post_date DESC");
			if( $years ):
				foreach( $years as $year ):
					if( isset( $postyear ) ):
						if(  $postyear  == $year ):
							$classyear = 'activeyear';
							$classcollapse = 'in';
						else:
							$classyear = '';
							$classcollapse = '';							
						endif;
					else:
						$classyear = '';
						$classcollapse = '';
					endif;
					?>
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<a class="raleway easeme year <?php echo $classyear; ?>" href="<?php echo get_post_type_archive_link( get_post_type() ).$year ?>/">
								<?php echo $year; ?>
							</a>
						</div><!--/.panel-heading -->

					</div><!-- /.panel --> 
					
				<?php endforeach;
			endif; ?>
		</div> <!-- end of panel-group -->
	</div> <!-- end of panel-body -->
</div> <!-- end of panel-collapse -->

