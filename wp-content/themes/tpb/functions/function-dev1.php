<?php
if (!function_exists('ap_helper_underconstruction_shortcode')):
	function ap_helper_underconstruction_shortcode($atts, $content) {
		global $post;
		$children = get_pages( array(
			'child_of' 	=> $post->ID,
			'sort_order'		=> 'ASC',
			'sort_column'  => 'menu_order'
		 	) );
		if( $children ):
			foreach( $children as $child ):
				wp_redirect( get_the_permalink( $child->ID ) );
				// var_dump( get_the_permalink($post->ID) );
				break;
			endforeach;
		endif;
	}
	add_shortcode( 'redirect_to_child', 'ap_helper_underconstruction_shortcode' );
endif;



if (!function_exists('button_shortcode')):
function button_shortcode($atts, $content) {
    ob_start();
    $d = array(
        "title"=> "submit"
    );
    $o = array_merge($d, $atts);    
    echo '<a href="'.$o["link"].'" class="btn easeme downloadbtn raleway" target="_blank">Download Files <i class="fa fa-arrow-down" aria-hidden="true"></i></a>';
    return ob_get_clean();
}
add_shortcode( 'download', 'button_shortcode' );
endif;


if (!function_exists('starting_id')):
function starting_id($atts, $content) {
    ob_start();
    $d = array(
        "title"=> "submit"
    );
    $o = array_merge($d, $atts);    
    echo '<div id="'.$o["id"].'">';
    return ob_get_clean();
}
add_shortcode( 'id_start', 'starting_id' );
endif;


if (!function_exists('ending_id')):
function ending_id($atts, $content) {
    ob_start();
    echo '</div>';
    return ob_get_clean();
}
add_shortcode( 'id_end', 'ending_id' );
endif;

