<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="welc-wrap">
			<?php 
				$id = get_id_using_template("templates/welcome.php");
				$welcome = get_post($id);
				echo $welcome->post_content;
			?>				
			</div>			
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<?php if( get_field( 'pages_hompage', 'options') ):
			$pages_hompage = get_field( 'pages_hompage', 'options');
			$countmein = 0;
			foreach( $pages_hompage as $p ):
				$countmein++;
			endforeach;
			if( $countmein == 1 ):
				$classtobe = 'col-sm-12';
			elseif( $countmein == 2 ):
				$classtobe = 'col-sm-6';
			elseif( $countmein == 3 ):
				$classtobe = 'col-sm-4';
			else:
				$classtobe = 'col-sm-3';				
			endif;
			foreach( $pages_hompage as $p ):
				if( $p['page'] ):
					$url = $p['page'];
				else:
					$url = site_url();
				endif;
				if( $p['background'] == 'color' ):
					$style = 'background:'.$p['color'].'; ';
				else:
					$style = 'background: url('.$p['Image'].') center 0px no-repeat; background-size:cover; -o-background-size: cover; -moz-background-size: cover; -webkit-background-size: cover'; 
				endif;
				?>
				<div class="<?php echo $classtobe; ?> custom-padding">
					<div class="common-events easeme" style='<?php echo $style; ?>'>
						<a href="<?php echo $url; ?>" class="easeme" ><?php echo $p['title'] ?></a>						
					</div>
				</div>
		<?php endforeach;
		endif; ?>
	</div>
</div>
<div class="container home-new">
	<div class="row">
		<div class="col-lg-12">
			<div class="whts-new">
				<h2 class="text-center">WHAT'S NEW</h2>
			</div>
		</div>
		<?php 
		$args = array(
		    'post_type'      => 'whats-new',

		    'posts_per_page' => 2,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key'	=> 'featured',
					'value'=> 1,
					'type' => 'NUMERIC',
					'compare' => '=', 
				),
			),
		);
		$variables = new WP_Query($args);

		if( $variables->have_posts() ):
			while( $variables->have_posts() ):  $variables->the_post();
				if( has_post_thumbnail() ):
					$image = wp_get_attachment_url( get_post_thumbnail_id() );
				else:
					$image = get_bloginfo('template_url')."/images/dummyimg2.png";
				endif; ?>
				<div class="col-lg-6 col-md-6">
					<div class="media whats-new-item">
						<div class="row">
							<div class="col-xs-5 futurewhatsnew">
								<div class="img-center center">
									<div class="whatsnewimg img-center" style="background: url(<?php echo $image; ?>) center 0px no-repeat; background-size:cover; -o-background-size: cover; -moz-background-size: cover; -webkit-background-size: cover; ">
										<a href="<?php the_permalink(); ?>">									
										</a>								
									</div>									
								</div>
							</div>
							<div class="col-xs-7 futurewhatsnew">
								<h4 class="media-heading"><?php the_title(); ?></h4>
								<p class='date-whats-new text-italic'><em>Date: <?php the_time('F d, Y') ?></em></p>
								<p class='content-whats-new'>
									<?php
									$myExcerpt = get_the_excerpt();
									$tags = array("<p>", "</p>");
									$myExcerpt = str_replace($tags, "", $myExcerpt);
									$strcount = strlen($myExcerpt);
									$newexcerpt = substr($myExcerpt, 0,250);
									if ($strcount <= 250):
										echo $myExcerpt;
									else:
										echo $newexcerpt.'...';
									endif;
									?>
								</p>
								<a href="<?php the_permalink(); ?>" class="default-link-btn easeme">READ MORE</a>							
							</div>	<!-- end of media-body -->						
						</div> <!-- end of row -->
					</div> <!-- end of whats-new-item -->		
				</div>
			<?php endwhile; wp_reset_postdata();
		endif; ?>	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="fullwidth center">
				<a href="<?php echo get_post_type_archive_link('whats-new'); ?>" class="default-link-btn post-perma easeme"><b>READ ALL</b></a>			
			</div>
		</div>		
		<div class="clearfix"></div>
	</div>
</div>
<br>
<?php get_footer(); ?>