<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-5 nopaddingright">
				<div class="footerheight table-responsive wysi-p">
					<?php if( get_field( 'contact_information', 'options' ) ):
						echo get_field( 'contact_information', 'options' );
					endif; ?>
				</div>
			</div> <!-- end of col-lg-5 -->
			<div class="col-md-2 col-sm-3">
				<div class="footerheight footer-links">
					<ul>
						<?php
						$menuargs = array(
						'theme_location'    =>  'footer-menu',
						'container'         =>  '',
						'menu_class'        =>  '',
						'menu_id'           =>  '',
						'items_wrap'      => '%3$s',
						);
						wp_nav_menu($menuargs); ?>
					</ul>
				</div>
			</div> <!-- end of col-lg-2 -->
			<div class="col-md-2 col-sm-4 nopadding">
				<div class="footerheight">
					<div class="table-responsive wysi-p">
						<?php if( get_field( 'quick_links', 'options' ) ):
							echo get_field( 'quick_links', 'options' );
						endif; ?>
					</div>
					<div class="table-responsive add-margin-top">
						<?php if( get_field( 'social_media_accounts', 'options' ) ): ?>
							<span style="display: block; margin-bottom: 7px; " class="mehover"><b style="color:#fff;">CONNECT WITH US</b></span>
							<?php $smas =  get_field( 'social_media_accounts', 'options' );
							$sctr = 0;
							foreach( $smas as $sma ): ?>
								<a class="sm-link" href="<?php echo $sma['url']; ?>" target="_blank"><img src="<?php echo $sma['inactive'];  ?>" alt="social" class="easeme" id="social<?php echo $sctr; ?>"></a>

							<script type="text/javascript">
								jQuery(window).load(function($){
									jQuery('#social<?php echo $sctr; ?>').on({
									    'mouseover': function(){
									        jQuery('#social<?php echo $sctr; ?>').attr('src','<?php echo $sma['active'];  ?>');
									    }
									});
									jQuery('#social<?php echo $sctr; ?>').on({
									    'mouseleave': function(){
									        jQuery('#social<?php echo $sctr; ?>').attr('src','<?php echo $sma['inactive'];  ?>');
									    }
									});
								});
							</script>

							<?php $sctr++; endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div> <!-- end of col-lg-3 -->
			<div class="col-md-3 col-xs-12 logos">
				<div class="footerheight table-responsive center">
					<?php if( get_field( 'logos', 'options' ) ):
						echo get_field( 'logos', 'options' );
					endif; ?>
				</div>
			</div> <!-- end of col-lg-2 -->
			<div class="col-xs-12">
				<div class="g-footer center">
					<p>Copyright 2015. <span style="color: #fbf005; ">Tourism Promotions Board</span>. All Rights Reserved.</p>
					<p>Powered by: ZOOM Hosting and <a href="http://zoom.ph/" target="_blank">Domain Registration</a>. Design by <a href="http://www.iconcept.com.ph/" target="_blank">Web Design Philippines</a>. SEO by <a href="http://www.iconcept-seo.com/" target="_blank">SEO Philippines</a> </p>
				</div>
			</div>
		</div> <!-- end of row -->
	</div>
	<a href="#top" class="js-to-top h-fx"><i class="fa fa-angle-up"></i></a>
 <!-- end of container -->
</footer>

<?php wp_footer(); ?>
	<script type="text/javascript">
		jQuery(window).load(function($){
	    	var biggestHeight = 0;
		    jQuery('.footerheight').each(function(){
			    if(jQuery(this).height() > biggestHeight){
		    		biggestHeight = jQuery(this).height() ;
		    	}
		    });
			jQuery('.footerheight').height(biggestHeight);
		});
		$(document).ready(function () {
			$("#headermenu2").mmenu({
				classes: "mm-light",
				offCanvas: {
					position: "right",
					zposition : "front",
				}
			});
			//  to top
			$("a[href='#top']").click(function() {
				$("html, body").animate({ scrollTop: 0 }, "slow");
				return false;
			});
			
			$('#careerslider').easyResponsiveTabs({
				tabidentify: 'vert',
				type: 'vertical',
			} );

			$('#invitation').easyResponsiveTabs({
				tabidentify: 'vert',
				type: 'accordion',
				activetab_bg: '#19a6d9',
				inactive_bg : '#fff',
				active_border_color : 'transparent',
				active_content_border_color : 'transparent',
			} );

			$('#qaslider').easyResponsiveTabs({
				tabidentify: 'vert',
				type: 'accordion',
				activetab_bg: '#19a6d9',
				inactive_bg : '#fff',
				active_border_color : 'transparent',
				active_content_border_color : 'transparent',

			} );

			$(window).load(function() {
				var header_height = $('#header').outerHeight();
				if( $(window).scrollTop() >= 100 ) {
					$('.js-to-top').stop().fadeIn();
				} else {
					$('.js-to-top').stop().fadeOut();
				}
			});

			$(window).on('scroll', function() {
				var header_height = $('#header').outerHeight();
				if( $(window).scrollTop() >= 430 ) {
					$('.js-to-top').stop().fadeIn('fast');
				} else {
					$('.js-to-top').stop().fadeOut('fast');
				}
			});

			$('.pdfbuttonid').on( 'click', function(e){
				e.preventDefault();
				$('#formbutton').submit();

			});

			var $j = jQuery.noConflict();

			var realSlider= $j("ul#csrmainslide").bxSlider({
			      speed:1000,
			      pager:false,
			      nextText:'',
			      prevText:'',
			      controls: false,
			      infiniteLoop:false,
			      hideControlOnEnd:true,
			      onSlideBefore:function($slideElement, oldIndex, newIndex){
			        changeRealThumb(realThumbSlider,newIndex);
			        
			      }
			      
			    });
			    
			    var realThumbSlider=$j("ul#csrslide").bxSlider({
			      minSlides: 4,
			      maxSlides: 4,
			      slideWidth: 156,
			      slideMargin: 12,
			      moveSlides: 1,
			      pager:false,
			      speed:1000,
			      infiniteLoop:false,
			      hideControlOnEnd:true,
			      nextText:'<span></span>',
			      prevText:'<span></span>',
			      onSlideBefore:function($slideElement, oldIndex, newIndex){
			        /*$j("#sliderThumbReal ul .active").removeClass("active");
			        $slideElement.addClass("active"); */

			      }
			    });
			    
			    linkRealSliders(realSlider,realThumbSlider);
			    
			    if($j("#csrslide li").length<5){
			      $j("#csrslide .bx-next").hide();
			    }

			// sincronizza sliders realizzazioni
			function linkRealSliders(bigS,thumbS){
			  
			  $j("ul#csrslide").on("click","a",function(event){
			    event.preventDefault();
			    var newIndex=$j(this).parent().attr("data-slideIndex");
			        bigS.goToSlide(newIndex);
			  });
			}

			//slider!=$thumbSlider. slider is the realslider
			function changeRealThumb(slider,newIndex){
			  
			  var $thumbS=$j("#csrslide");
			  $thumbS.find('.active').removeClass("active");
			  $thumbS.find('li[data-slideIndex="'+newIndex+'"]').addClass("active");
			  
			  if(slider.getSlideCount()-newIndex>=4)slider.goToSlide(newIndex);
			  else slider.goToSlide(slider.getSlideCount()-4);

			}


		});
	</script>
	</body>
</html>
