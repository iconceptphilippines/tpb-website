<?php  
require_once locate_template('cus-post-type.php');
require_once locate_template('backend-customization.php');
require_once locate_template('functions/function-dev1.php');
require_once locate_template('functions/function-dev2.php');
require_once locate_template('functions/function-dev3.php');


// ================== ADD EXCERPT ==================================
function connect_addexcerpt() {
    add_meta_box('postexcerpt', __('Excerpt'), 'post_excerpt_meta_box', 'page', 'normal', 'core');
}
add_action( 'admin_menu', 'connect_addexcerpt' );

remove_filter ('the_content', 'wpautop');

// ===================== styles and js ==============================
add_image_size( 'custom-size', 97, 89 );



// ===================== styles and js ==============================


function custom_theme_scripts() {
    
    wp_enqueue_style( 'tpb_style', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '', 'all' );
    wp_enqueue_style( 'css1', get_template_directory_uri() . '/css/dev1.css', array(), '', 'all' );
    wp_enqueue_style( 'css2', get_template_directory_uri() . '/css/dev2.css', array(), '', 'all' );
    wp_enqueue_style( 'css3', get_template_directory_uri() . '/css/dev3.css', array(), '', 'all' );
    wp_enqueue_style( 'bxslider-css', get_template_directory_uri() . '/css/jquery.bxslider.css', array(), '', 'all' );
    wp_enqueue_style( 'mmenu', get_template_directory_uri() . '/css/jquery.mmenu.all.css', array(), '', 'all' );
    wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '', 'all' );
    wp_enqueue_style( 'responsivetabs', get_template_directory_uri() . '/css/easy-responsive-tabs.css', array(), '', 'all' );
    wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', array(), '', 'all' );




    wp_enqueue_script( 'jquery_js', get_template_directory_uri() . '/js/jquery.min.js', array(), '', true );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'bxslider-js', get_template_directory_uri() . '/js/jquery.bxslider.min.js', array(), '', true );
    wp_enqueue_script( 'mmenu-js', get_template_directory_uri() . '/js/jquery.mmenu.all.min.js', array(), '', true );
    wp_enqueue_script( 'responsivetabs-js', get_template_directory_uri() . '/js/easyResponsiveTabs.js', array(), '', true );
    wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/js/jquery.fancybox.js', array(), '', true );



}
add_action( 'wp_enqueue_scripts', 'custom_theme_scripts' );



//============= insert font to wysiwyg ========================================
if ( ! function_exists( 'wpex_mce_google_fonts_array' ) ) {
    function wpex_mce_google_fonts_array( $initArray ) {
        $initArray['font_formats'] = 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Open Sans=Open Sans,sans-serif;Open Sans Condensed=Open Sans Condensed,sans-serif;Raleway=Raleway,sans-serif;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats';
            return $initArray;
    }
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_google_fonts_array' );

if ( ! function_exists( 'wpex_mce_google_fonts_styles' ) ) {
    function wpex_mce_google_fonts_styles() {
       $font_url = 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800';
           add_editor_style( str_replace( ',', '%2C', $font_url ) );
       $font_url = 'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700';
           add_editor_style( str_replace( ',', '%2C', $font_url ) );
       $font_url = 'https://fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,300italic,500italic,600italic,700,600,700italic,800,800italic,900,900italic';
           add_editor_style( str_replace( ',', '%2C', $font_url ) );
    }
}
add_action( 'init', 'wpex_mce_google_fonts_styles' );


//============= insert font size to wysiwyg ========================================
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
    function wpex_mce_text_sizes( $initArray ){
        $initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 15px 16px 18px 20px 21px 22px 24px 28px 32px 36px 38px 40px 45px 50px";
        return $initArray;
    }
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );



// ================= naming ===================================================
function custom_wp_title( $title, $sep ) {
    if ( is_feed() ) {
        return $title;
    }

    global $page, $paged;
    
    // Add the blog name

    $title .= get_bloginfo( 'name', 'display' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description";
    }

    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
    }

    return $title;
}
add_filter( 'wp_title', 'custom_wp_title', 10, 2 );


// ==================== add featured image ============================
add_theme_support('post-thumbnails');

// ====================== add options page ===================================
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Other Settings',
        'menu_title'    => 'Other Settings',
        'menu_slug'     => 'other-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

}

// ====================== add options page mice international ===================================
acf_add_options_sub_page(array(
    'title' => 'Mice International Page Settings',
    'parent' => 'edit.php?post_type=mice-international',
    'slug' => 'mice-international-page-settings',
    'menu' => 'Mice International Settings'
));

// ====================== add options page mice national ===================================
acf_add_options_sub_page(array(
    'title' => 'Mice National Page Settings',
    'parent' => 'edit.php?post_type=mice-national',
    'slug' => 'mice-national-page-settings',
    'menu' => 'Mice National Settings'
));

// ====================== add options page alpha listing ===================================
acf_add_options_sub_page(array(
    'title' => 'Alpha Listing Page Settings',
    'parent' => 'edit.php?post_type=alpha-listing',
    'slug' => 'alpha-listing-page-settings',
    'menu' => 'Alpha Listing Settings'
));


// ====================== add options page invitation to bid ===================================
acf_add_options_sub_page(array(
    'title' => 'Invitation to Bid Page Settings',
    'parent' => 'edit.php?post_type=invitation-to-bid',
    'slug' => 'invitation-to-bid-page-settings',
    'menu' => 'Invitation to Bid Settings'
));


// ====================== add options page whats new ===================================
acf_add_options_sub_page(array(
    'title' => 'Whats New Page Settings',
    'parent' => 'edit.php?post_type=whats-new',
    'slug' => 'whats-new-page-settings',
    'menu' => 'Whats New Settings'
));



// ====================== add menus ===================================
if ( ! function_exists('tpb_theme_menus') ) {

// Register Navigation Menus
function tpb_theme_menus() {
    $locations = array(
        'main-menu' => __( 'Main Menu', 'tpb' ),
        'footer-menu' => __( 'Footer Menu', 'tpb' ),

    );
    register_nav_menus( $locations );
}
// Hook into the 'init' action
add_action( 'init', 'tpb_theme_menus' );
}



//=========== GET TEMPLATE URL ====================

if ( !function_exists('get_id_using_template')) {
    function get_id_using_template( $path ) {
        global $wpdb;
        $pages = $wpdb->get_col( $wpdb->prepare ("
                    SELECT post_id
                    FROM $wpdb->postmeta
                    WHERE meta_key = '_wp_page_template' AND meta_value = %s",
                    $path
        ) );
        if($pages):
            foreach($pages as $page):
                return $page;
            endforeach;
        else:
            return 0;
        endif;
    }
};

function get_post_excerpt_by_id( $post_id ) {
    global $post;
    $post = get_post( $post_id );
    setup_postdata( $post );
    $the_excerpt = get_the_excerpt();
    wp_reset_postdata();
    return $the_excerpt;
}

if ( !function_exists( 'tpb_pagination' ) ) {
    
    function tpb_pagination() {
        
        global $variable;
        $total = $variable->max_num_pages;
        $big = 999999999; // need an unlikely integer
        global $variable;

        $big = 999999999; // need an unlikely integer

        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '<div class="btn-btn-danger">?paged=%#%</div>',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $variable->max_num_pages,
            'prev_text'    => __('<i class="fa fa-angle-left" aria-hidden="true"></i> Previous'),
            'next_text'    => __('Next <i class="fa fa-angle-right" aria-hidden="true"></i>'),
            'mid_size'     => 1,
            'end_size'     => 1,
        ) );
    }   
}


if ( !function_exists( 'tpb_pagination2' ) ) {
    
    function tpb_pagination2() {
        
        global $variable;
        $total = $variable->max_num_pages;
        $big = 999999999; // need an unlikely integer
        global $variable;

        $big = 999999999; // need an unlikely integer

        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '<div class="btn-btn-danger">?paged=%#%</div>',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $variable->max_num_pages,
            'prev_text'    => __('<i class="fa fa-angle-left" aria-hidden="true"></i> Previous'),
            'next_text'    => __('Next <i class="fa fa-angle-right" aria-hidden="true"></i>'),
            'mid_size'     => 1,
            'end_size'     => 1,
        ) );
    }   
}

if ( !function_exists( 'has_child_pages' ) ) {
    
    function has_child_pages( $postid ) {
        $children = get_pages('child_of='.$postid);
        if( count( $children ) != 0 ){
            return true;
        } else { 
            return false;
        }
    }
    
}



if ( !function_exists( 'tpb_archive_pagination' ) ) {
    
    function tpb_archive_pagination() {
        
        global $wp_query;
        $total = $wp_query->max_num_pages;
        $big = 999999999; // need an unlikely integer
        global $wp_query;

        $big = 999999999; // need an unlikely integer

        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '<div class="btn-btn-danger">?paged=%#%</div>',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'prev_text'    => __('<i class="fa fa-angle-left" aria-hidden="true"></i> Previous'),
            'next_text'    => __('Next <i class="fa fa-angle-right" aria-hidden="true"></i>'),
            'mid_size'     => 1,
            'end_size'     => 1,
        ) );
    }   
}


// ============ force published for mice national =============
function publish_as_future_international( $post_data ) {
    if ( $post_data['post_status'] == 'future' && $post_data['post_type'] == 'mice-international' ) {
        $post_data['post_status'] = 'publish';
    } elseif( $post_data['post_status'] == 'future' && $post_data['post_type'] == 'mice-national' ){
        $post_data['post_status'] = 'publish';
    } elseif( $post_data['post_status'] == 'future' && $post_data['post_type'] == 'csr' ){
        $post_data['post_status'] = 'publish';
    } 

    return $post_data;
}
add_filter('wp_insert_post_data', 'publish_as_future_international');
remove_action('future_post', '_future_post_hook');


// ------------------- AVATAR ON COMMENT --------------
add_action( 'comment_form_logged_in_after', 'psot_comment_form_avatar' );
add_action( 'comment_form_after_fields', 'psot_comment_form_avatar' );
function psot_comment_form_avatar() {
    if( is_user_logged_in() ): ?>
       <div class="comment-avatar pull-left">
         <?php 
         $current_user = wp_get_current_user();
         if ( ($current_user instanceof WP_User) ) {
            echo get_avatar( $current_user->user_email, 50, null, null, array( 'class' => array( 'img-responsive', 'img-center' ) ) );
         }
         ?>
        </div>
    <?php endif;
}


// ========================= start rewriting of rules =====================

function tpb_flush_rules(){
    $rules = get_option( 'rewrite_rules' );
    
    if ( ! isset( $rules['(invitation-to-bid)/([^/]*)/([^/]*)$'] ) && ! isset( $rules['(invitation-to-bid)/([^/]*)/([^/]*)/page/([0-9]+)$'] ) ) {
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }
}

add_action( 'wp_loaded','tpb_flush_rules' );

function tpb_flush_rules2(){
    $rules = get_option( 'rewrite_rules' );
    
    if ( ! isset( $rules['(whats-new)/([^/]*)/([^/]*)$'] ) && ! isset( $rules['(whats-new)/([^/]*)/([^/]*)/page/([0-9]+)$'] ) ) {
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }
}

add_action( 'wp_loaded','tpb_flush_rules2' );


function tpb_rewrite_rules( $rules ) {
    $newrules = array();

    $newrules['(invitation-to-bid)/page/([0-9]+)$'] = 'index.php?post_type=$matches[1]&paged=$matches[2]';
    $newrules['(invitation-to-bid)/([0-9][^/]*)$'] = 'index.php?post_type=$matches[1]&postyear=$matches[2]';
    $newrules['(invitation-to-bid)/([^/]*)/page/([0-9]+)$'] = 'index.php?post_type=$matches[1]&postyear=$matches[2]&paged=$matches[3]';
    $newrules['(invitation-to-bid)/([^/]*)/([^/]*)$'] = 'index.php?post_type=$matches[1]&postyear=$matches[2]&postmonth=$matches[3]';
    $newrules['(invitation-to-bid)/([^/]*)/([^/]*)/page/([0-9]+)$'] = 'index.php?post_type=$matches[1]&postyear=$matches[2]&postmonth=$matches[3]&paged=$matches[4]';
    
    return $newrules + $rules;
}

add_filter( 'rewrite_rules_array','tpb_rewrite_rules' );


function tpb_rewrite_rules2( $rules ) {
    $newrules = array();

    $newrules['(whats-new)/page/([0-9]+)$'] = 'index.php?post_type=$matches[1]&paged=$matches[2]';
    $newrules['(whats-new)/([0-9][^/]*)$'] = 'index.php?post_type=$matches[1]&postyear=$matches[2]';
    $newrules['(whats-new)/([^/]*)/page/([0-9]+)$'] = 'index.php?post_type=$matches[1]&postyear=$matches[2]&paged=$matches[3]';
    $newrules['(whats-new)/([^/]*)/([^/]*)$'] = 'index.php?post_type=$matches[1]&postyear=$matches[2]&postmonth=$matches[3]';
    $newrules['(whats-new)/([^/]*)/([^/]*)/page/([0-9]+)$'] = 'index.php?post_type=$matches[1]&postyear=$matches[2]&postmonth=$matches[3]&paged=$matches[4]';
    
    return $newrules + $rules;
}

add_filter( 'rewrite_rules_array','tpb_rewrite_rules2' );



function tpb_insert_query_vars( $vars ) {
    array_push($vars, 'postyear');
    array_push($vars, 'postmonth');
    return $vars;
}

add_filter( 'query_vars','tpb_insert_query_vars' );


function tpb_insert_query_vars2( $vars ) {
    array_push($vars, 'postyear');
    array_push($vars, 'postmonth');
    return $vars;
}

add_filter( 'query_vars','tpb_insert_query_vars2' );



function tpb_queryposts($query){
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if( $query->is_main_query() && is_post_type_archive('invitation-to-bid') ) {
        $query->set( 'posts_per_page', 9 );
        if( get_query_var( 'postyear' ) || get_query_var( 'postmonth' ) ) :
            $query->set( 'year', get_query_var( 'postyear' ) );
            $query->set( 'monthnum', get_query_var( 'postmonth' ) );
        endif;
        if( get_query_var('paged') ) :
            $query->set( 'paged', get_query_var('paged') );
        endif;
        return;
    }
    if( $query->is_main_query() && is_post_type_archive('whats-new') ) {
        $query->set( 'posts_per_page', 9 );
        if( get_query_var( 'postyear' ) || get_query_var( 'postmonth' ) ) :
            $query->set( 'year', get_query_var( 'postyear' ) );
            $query->set( 'monthnum', get_query_var( 'postmonth' ) );
        endif;
        if( get_query_var('paged') ) :
            $query->set( 'paged', get_query_var('paged') );
        endif;
        return;
    }
}

add_action('pre_get_posts', 'tpb_queryposts',1);



function get_content_by_id( $postid ){
    $content_post = get_post($postid);
    $content = $content_post->post_content;
    
    if( $content == '<p>[redirect_to_child]</p>' || $content == '[redirect_to_child]' ):
        return false;
    elseif( $content ):
        return $content;
    else:
        return false;
    endif;
}
