<?php get_header(); ?>

<div id="si-page">
	<?php
		$notfoundid = get_id_using_template('templates/template-not-found.php');
		$content_post = get_post($notfoundid);
		$content = $content_post->post_content;
		echo '<div class="container" style="padding:30px 15px;">'; ?>
			<div class="row">
				<div class="table-ronsponsive">
					<?php
					$content = apply_filters('the_content', $content);
					echo $content; 
					?>
				</div> <!-- end of table-responsive -->
			</div> <!-- end of row -->
		</div> <!-- end of container -->
	<?php  ?>
</div> <!-- end of si-page -->

<?php get_footer(); ?>