<?php get_header(); ?>

<div id="archive">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php echo get_sidebar(); ?>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="invitation">
						<?php if( have_posts() ): ?>
							<div id="invitation">
								<ul class="resp-tabs-list vert">
									<?php while( have_posts() ): the_post();
										if( has_post_thumbnail() ):
											$image = wp_get_attachment_url( get_post_thumbnail_id() );
										else:
											$image = get_bloginfo('template_url')."/images/dummyimg2.png";
										endif;
										?>
										<li>
											<div class="row">
												<div class="col-sm-2 col-xs-3">
													<img src="<?php echo $image ?>" alt="image" class="img-responsive img-center">
												</div>
												<div class="col-sm-10 col-xs-9 nopadding">
													<div class="title ralewayimp">
														<?php the_title(); ?>														
													</div>
												</div>
											</div>
										</li>
									<?php endwhile; wp_reset_postdata();  ?>
								</ul> <!-- end of resp-tabs-list -->
								
								<div class="resp-tabs-container vert">
									<?php while( have_posts() ): the_post();
										if( has_post_thumbnail() ):
											$image = wp_get_attachment_url( get_post_thumbnail_id() );
										else:
											$image = get_bloginfo('template_url')."/images/dummyimg2.png";
										endif; ?>
										<div>
											<div class="row">
												<?php if(get_field('file')):
													foreach( get_field('file') as $solo ): ?>
														<div class="col-lg-12 col-sm-12">
															<div class="row">			
																<div style="margin-bottom:15px; display: -webkit-box;">
																	<div class="col-sm-1 col-xs-2" style="width: 12% !important;">
																		<img src="<?php echo get_bloginfo('template_url').'/images/pdflogo.png' ?>" alt="icon" class="img-responsive pull-right">
																		<div class="clearfix"></div>
																	</div>
																	<div class="col-sm-11 col-xs-10" style="width: 83% !important;">
																		<div class="title ralewayimp">
																			<?php echo $solo['title']; ?>														
																		</div>
																		<p class="brochure-link"><a download target="_blank" href="<?php echo $solo['url']; ?>"><i class="fa fa-download" aria-hidden="true"></i>Download</a><a target="_blank" href="<?php echo $solo['url']; ?>"><i class="fa fa-eye" aria-hidden="true"></i>View</a></p>
																	</div>																	
																</div>								
															</div>
														</div>
													<?php endforeach;
												endif; ?>
											</div>	
										</div>
									<?php endwhile; wp_reset_postdata();  ?>
								</div>
							</div>	
						<?php endif; ?>
					</div>
					<div class="col-xs-12">
						<div class="pagination fullwidth">
							<div class="right">
								<?php echo tpb_archive_pagination(); ?>									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- end of container -->
</div> <!-- end of archive -->

<?php get_footer(); ?>