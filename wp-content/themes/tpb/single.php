<?php
ob_start();
get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive">
						<?php if( is_singular( 'events' ) ):
							if( get_field( 'single_page_content' ) ):
								echo get_field( 'single_page_content' ) ;
							endif;
						else:
							the_content();
						endif;?>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>