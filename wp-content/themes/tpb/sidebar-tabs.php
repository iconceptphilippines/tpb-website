<?php
$currentid = get_the_id();

$ancestor = get_post_ancestors($currentid);
$depth = count( $ancestor );

$parent0 = false;
$parent1 = false;
$parent2 = false;
$parent3 = false;

if( $depth == 0 || $depth == 1 ):
	if( $post->post_parent ):
		$parent0 = $post->post_parent ;
	else:
		$parent0 = get_the_id();
	endif;

	$childargs = array(
	    'post_type'      => 'page',
	    'posts_per_page' => -1,
	    'post_parent'    => $parent0,
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
		);
	$child = new WP_Query($childargs);
	if( $child->have_posts() ): ?>
		<div class="panel-group" id="globalslider">

			<?php while( $child->have_posts() ): $child->the_post();
				if( get_the_id() == $currentid ):
					$class = 'active';
				else:
					$class = '';
				endif; ?>
				<div class="panel panel-default">
					<div class="panel-heading <?php echo $class; ?>">
						<?php if( has_child_pages( $post->ID ) ):
							if( !get_content_by_id( $post->ID ) ): ?>
								<a href="#<?php echo $post->ID; ?>" data-toggle="collapse" data-parent="#globalslider" class="<?php echo $class; ?> title1 raleway easeme buttoninside">
									<?php the_title(); ?>
								</a>
								<span class="clearfix"></span>
							<?php else: ?>
								<a href="<?php the_permalink(); ?>" class="downbuttonlink raleway easeme title1 <?php echo $class; ?>">
									<span class="title"><?php the_title(); ?></span>
								</a>
								<a href="#<?php echo $post->ID; ?>" class="raleway easeme title1 pull-right downbutton <?php echo $class; ?>" data-toggle="collapse" data-parent="#globalslider"> <i class="fa" aria-hidden="true"></i> </a>
								<div class="clearfix"></div>
							<?php endif;
						else: ?>								
							<a href="<?php the_permalink(); ?>" class="title1 raleway easeme buttoninside <?php echo $class; ?>"><?php the_title(); ?></a>
						<?php endif; ?>
					</div><!--/.panel-heading -->
					<div id="<?php echo $post->ID; ?>" class="panel-collapse collapse body1">
						<div class="panel-body">

							<div class="panel-group" id="nested<?php echo $post->ID; ?>">
								<div class="panel panel-default">
									<?php
									$parentpage = $post->ID; 
									$childargs2 = array(
									    'post_type'      => 'page',
									    'posts_per_page' => -1,
									    'post_parent'    => get_the_ID(),
									    'order'          => 'ASC',
									    'orderby'        => 'menu_order'
										);
									$child2 = new WP_Query($childargs2);
									if( $child2->have_posts() ): ?>
										<?php while( $child2->have_posts() ): $child2->the_post();
											if( $post->ID == $currentid ):
												$class = 'active2';
											else:	
												$class = '';
											endif; ?>

											<div class="panel-heading">
												<?php if( get_id_using_template('templates/template-csr.php') == $post->ID ): ?>
													<a href="<?php the_permalink(); ?>?content" class="downbuttonlink raleway easeme title2 <?php echo $class; ?>">
														<span class="title"><?php the_title(); ?></span>
													</a>
													<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2 pull-right downbutton collapsed <?php echo $class; ?>" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"> <i class="fa" aria-hidden="true"></i> </a>
													<div class="clearfix"></div>
												<?php elseif( !has_child_pages( $post->ID ) ): ?>
													<a href="<?php the_permalink(); ?>" class="raleway easeme title2 <?php echo $class; ?>">
														<span class="title"><?php the_title(); ?></span>
													</a>
												<?php else:
													if( get_id_using_template('templates/template-mice-calendar.php') == $post->ID ):?>
														<a href="<?php the_permalink(); ?>" class="downbuttonlink raleway easeme title2 <?php echo $class; ?>">
															<span class="title"><?php the_title(); ?></span>
														</a>
														<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2 pull-right downbutton collapsed <?php echo $class; ?>" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"> <i class="fa" aria-hidden="true"></i> </a>
														<div class="clearfix"></div>
													<?php else: ?>	
														<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"><?php the_title(); ?></a>														
													<?php endif;
												endif; ?>
											</div><!--/.panel-heading -->

											<?php
											$postid2 = $post->ID; 
											$childargs3 = array(
											    'post_type'      => 'page',
											    'posts_per_page' => -1,
											    'post_parent'    => $post->ID,
											    'order'          => 'ASC',
											    'orderby'        => 'menu_order'
												);
											$child3 = new WP_Query($childargs3);
											if( $child3->have_posts() ): $sctr = 1; ?>
												<div id="nested<?php echo $postid2; ?>" class="panel-collapse collapse body2">
													<div class="panel-body">
														<div class="panel-group" id="postnested<?php echo $postid2; ?>">
															<?php while( $child3->have_posts() ): $child3->the_post(); ?>
																<div class="panel panel-default">
																	<div class="panel-heading">
																		<?php if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ): ?>
																			<a href="#nested<?php echo $post->ID.'-'.$sctr; ?>" class="raleway easeme title3" data-toggle="collapse" data-parent="#postnested<?php echo $postid2; ?>"><?php the_title(); ?>
																			<?php if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ):
																				echo '<span class="button pull-right right"><i class="fa" aria-hidden="true"></i></span>';	
																			endif; ?>										
																			</a>
																		<?php
																		elseif( !has_child_pages( $post->ID ) ): ?>
																			<a href="<?php the_permalink(); ?>" class="raleway easeme title3">
																				<span class="title"><?php the_title(); ?></span>
																			</a>
																		<?php endif; ?>
																	</div> <!-- end of panel-heading -->
																	<?php
																	if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ):
																		include(locate_template('partials/tabs-mice-calendar.php'));
																	endif; ?>
																</div> <!-- end of panel-default -->
															<?php $sctr++; endwhile; ?>
														</div> <!-- end of panel-group -->
													</div><!--/.panel-body -->
												</div><!--/.panel-collapse --> 
											<?php elseif( get_id_using_template('templates/template-csr.php') == $post->ID ):
												include(locate_template('partials/tabs-csr.php'));
											endif; ?>
										<?php endwhile; ?>
									<?php endif; ?>
								</div><!-- /.panel --> 
							</div><!-- nested -->

						</div><!--/.panel-body -->
					</div><!--/.panel-collapse -->
				</div><!-- /.panel -->
			<?php endwhile; ?>
		</div><!-- /.panel-group -->
	<?php endif; ?>

<?php elseif( $depth == 2 ):
	$childargs = array(
	    'post_type'      => 'page',
	    'posts_per_page' => -1,
	    'post_parent'    => $ancestor[1],
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
		);
	$child = new WP_Query($childargs);
	if( $child->have_posts() ): ?>
		<div class="panel-group" id="globalslider">
			<?php while( $child->have_posts() ): $child->the_post();
				if( $post->ID == $ancestor[0] ):
					$class = 'active';
					$class2 = 'in';
				else:	
					$class = '';
					$class2 = '';
				endif;
				?>
				<div class="panel panel-default">
					<div class="panel-heading <?php echo $class; ?>">
						<?php if( has_child_pages( $post->ID ) ):
							if( !get_content_by_id( $post->ID ) ): ?>
							<a href="#<?php echo $post->ID; ?>" data-toggle="collapse" data-parent="#globalslider" class="<?php echo $class; ?> title1 raleway easeme buttoninside">
								<?php the_title(); ?>
							</a>
							<span class="clearfix"></span>
							<?php else: ?>
								<a href="<?php the_permalink(); ?>" class="downbuttonlink raleway easeme title1 <?php echo $class; ?>">
									<span class="title"><?php the_title(); ?></span>
								</a>
								<a href="#<?php echo $post->ID; ?>" class="raleway easeme title1 pull-right downbutton <?php echo $class; ?>" data-toggle="collapse" data-parent="#globalslider"> <i class="fa" aria-hidden="true"></i> </a>
								<div class="clearfix"></div>
							<?php endif;
						else: ?>								
							<a href="<?php the_permalink(); ?>" class="title1 raleway easeme buttoninside <?php echo $class ?>"><?php the_title(); ?></a>
						<?php endif; ?>
					</div><!--/.panel-heading -->
					<div id="<?php echo $post->ID; ?>" class="panel-collapse collapse <?php echo $class2; ?> body1">
						<div class="panel-body">
							<div class="panel-group" id="nested<?php echo $post->ID; ?>">
								<div class="panel panel-default">
									<?php
									$parentpage = $post->ID; 
									$childargs2 = array(
									    'post_type'      => 'page',
									    'posts_per_page' => -1,
									    'post_parent'    => get_the_ID(),
									    'order'          => 'ASC',
									    'orderby'        => 'menu_order'
										);
									$child2 = new WP_Query($childargs2);
									if( $child2->have_posts() ): ?>
										<?php while( $child2->have_posts() ): $child2->the_post();
											if( $post->ID == $currentid ):
												$class = 'active2';
												$class2 = 'in';
											else:	
												$class = '';
												$class2 = '';
											endif; ?>

											<div class="panel-heading">
												<?php if( get_id_using_template('templates/template-csr.php') == $post->ID ): ?>
													<a href="<?php the_permalink(); ?>?content" class="downbuttonlink raleway easeme title2 <?php echo $class; ?>">
														<span class="title"><?php the_title(); ?></span>
													</a>
													<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2 pull-right downbutton collapsed <?php echo $class; ?>" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"> <i class="fa" aria-hidden="true"></i> </a>
													<div class="clearfix"></div>	
												<?php elseif( !has_child_pages( $post->ID ) ): ?>
													<a href="<?php the_permalink(); ?>" class="raleway easeme title2 <?php echo $class; ?>">
														<span class="title"><?php the_title(); ?></span>
													</a>
												<?php else:
													if( get_id_using_template('templates/template-mice-calendar.php') == $post->ID ):?>
														<a href="<?php the_permalink(); ?>" class="downbuttonlink raleway easeme title2 <?php echo $class; ?>">
															<span class="title"><?php the_title(); ?></span>
														</a>
														<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2 pull-right downbutton collapsed <?php echo $class; ?>" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"> <i class="fa" aria-hidden="true"></i> </a>
														<div class="clearfix"></div>
													<?php else: ?>	
														<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"><?php the_title(); ?></a>														
													<?php endif;
												endif; ?>
											</div><!--/.panel-heading -->
											<?php
											$postid2 = $post->ID; 
											$childargs3 = array(
											    'post_type'      => 'page',
											    'posts_per_page' => -1,
											    'post_parent'    => $post->ID,
											    'order'          => 'ASC',
											    'orderby'        => 'menu_order'
												);
											$child3 = new WP_Query($childargs3);
											if( $child3->have_posts() ): $sctr = 1; ?>
												<div id="nested<?php echo $postid2; ?>" class="panel-collapse collapse <?php echo $class2; ?> body2">
													<div class="panel-body">
														<div class="panel-group" id="postnested<?php echo $postid2; ?>">
															<?php while( $child3->have_posts() ): $child3->the_post(); ?>
																<div class="panel panel-default">
																	<div class="panel-heading">
																		<?php if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ): ?>
																			<a href="#nested<?php echo $post->ID.'-'.$sctr; ?>" class="raleway easeme title3" data-toggle="collapse" data-parent="#postnested<?php echo $postid2; ?>"><?php the_title(); ?>
																			<?php if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ):
																				echo '<span class="button pull-right right"><i class="fa" aria-hidden="true"></i></span>';	
																			endif; ?>										
																			</a>
																		<?php
																		elseif( !has_child_pages( $post->ID ) ): ?>
																			<a href="<?php the_permalink(); ?>" class="raleway easeme title3">
																				<span class="title"><?php the_title(); ?></span>
																			</a>
																		<?php endif; ?>
																	</div> <!-- end of panel-heading -->
																	<?php
																	if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ):
																		include(locate_template('partials/tabs-mice-calendar.php'));
																	endif; ?>
																</div> <!-- end of panel-default -->
															<?php $sctr++; endwhile; ?>
														</div> <!-- end of panel-group -->
													</div><!--/.panel-body -->
												</div><!--/.panel-collapse -->
											<?php elseif( get_id_using_template('templates/template-csr.php') == $post->ID ):
												include(locate_template('partials/tabs-csr.php'));
											endif; ?>
										<?php endwhile; ?>
									<?php endif; ?>
								</div> <!-- end of panel -->
							</div> <!-- end of panel -->
						</div> <!-- end of panel-body -->
					</div> <!-- end of panel -->
				</div> <!-- end of panel -->
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	<?php endif; ?>
<?php elseif( $depth == 3 ):
	$childargs = array(
	    'post_type'      => 'page',
	    'posts_per_page' => -1,
	    'post_parent'    => $ancestor[2],
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
		);
	$child = new WP_Query($childargs);
	if( $child->have_posts() ): ?>
		<div class="panel-group" id="globalslider">
			<?php while( $child->have_posts() ): $child->the_post();
				if( $post->ID == $ancestor[1] ):
					$class = 'active';
					$class2 = 'in';
				else:	
					$class = '';
					$class2 = '';
				endif;
				?>
				<div class="panel panel-default">
					<div class="panel-heading <?php echo $class; ?>">
						<?php if( has_child_pages( $post->ID ) ):
							if( !get_content_by_id( $post->ID ) ): ?>
							<a href="#<?php echo $post->ID; ?>" data-toggle="collapse" data-parent="#globalslider" class="<?php echo $class; ?> title1 raleway easeme buttoninside">
								<?php the_title(); ?>
							</a>
							<span class="clearfix"></span>
							<?php else: ?>
								<a href="<?php the_permalink(); ?>" class="downbuttonlink raleway easeme title1 <?php echo $class; ?>">
									<span class="title"><?php the_title(); ?></span>
								</a>
								<a href="#<?php echo $post->ID; ?>" class="raleway easeme title1 pull-right downbutton <?php echo $class; ?>" data-toggle="collapse" data-parent="#globalslider"> <i class="fa" aria-hidden="true"></i> </a>
								<div class="clearfix"></div>
							<?php endif;
						else: ?>								
							<a href="<?php the_permalink(); ?>" class="title1 raleway easeme buttoninside <?php echo $class ?>"><?php the_title(); ?></a>
						<?php endif; ?>
					</div><!--/.panel-heading -->
					<div id="<?php echo $post->ID; ?>" class="panel-collapse collapse <?php echo $class2; ?> body1">
						<div class="panel-body">
							<div class="panel-group" id="nested<?php echo $post->ID; ?>">
								<div class="panel panel-default">
									<?php
									$parentpage = $post->ID; 
									$childargs2 = array(
									    'post_type'      => 'page',
									    'posts_per_page' => -1,
									    'post_parent'    => get_the_ID(),
									    'order'          => 'ASC',
									    'orderby'        => 'menu_order'
										);
									$child2 = new WP_Query($childargs2);
									if( $child2->have_posts() ): ?>
										<?php while( $child2->have_posts() ): $child2->the_post();
											if( $post->ID == $ancestor[0] ):
												$class = 'active2';
												$class2 = 'in';
											else:	
												$class = '';
												$class2 = '';
											endif; ?>

											<div class="panel-heading">
												<?php if( get_id_using_template('templates/template-csr.php') == $post->ID ): ?>
													<a href="<?php the_permalink(); ?>?content" class="downbuttonlink raleway easeme title2 <?php echo $class; ?>">
														<span class="title"><?php the_title(); ?></span>
													</a>
													<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2 pull-right downbutton collapsed <?php echo $class; ?>" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"> <i class="fa" aria-hidden="true"></i> </a>
													<div class="clearfix"></div>
												<?php elseif( !has_child_pages( $post->ID ) ): ?>
													<a href="<?php the_permalink(); ?>" class="raleway easeme title2 <?php echo $class; ?>">
														<span class="title"><?php the_title(); ?></span>
													</a>
												<?php else:
													if( get_id_using_template('templates/template-mice-calendar.php') == $post->ID ):?>
														<a href="<?php the_permalink(); ?>" class="downbuttonlink raleway easeme title2 <?php echo $class; ?>">
															<span class="title"><?php the_title(); ?></span>
														</a>
														<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2 pull-right downbutton collapsed <?php echo $class; ?>" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"> <i class="fa" aria-hidden="true"></i> </a>
														<div class="clearfix"></div>
													<?php else: ?>	
														<a href="#nested<?php echo $post->ID; ?>" class="raleway easeme title2" data-toggle="collapse" data-parent="#nested<?php echo $parentpage; ?>"><?php the_title(); ?></a>														
													<?php endif;
												endif; ?>
											</div><!--/.panel-heading -->
											<?php
											$postid2 = $post->ID; 
											$childargs3 = array(
											    'post_type'      => 'page',
											    'posts_per_page' => -1,
											    'post_parent'    => $post->ID,
											    'order'          => 'ASC',
											    'orderby'        => 'menu_order'
												);
											$child3 = new WP_Query($childargs3);
											if( $child3->have_posts() ): $sctr = 1; ?>
												<div id="nested<?php echo $postid2; ?>" class="panel-collapse collapse <?php echo $class2; ?> body2">
													<div class="panel-body">
														<div class="panel-group" id="postnested<?php echo $postid2; ?>">
															<?php while( $child3->have_posts() ): $child3->the_post(); ?>
																<div class="panel panel-default">
																	<div class="panel-heading">
																		<?php if( $currentid == $post->ID ):
																			$class = 'active3';
																			$class2 = 'in';
																		else:
																			$class = '';
																			$class2 = '';
																		endif;

																		if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ): ?>
																			<a href="#nested<?php echo $post->ID.'-'.$sctr; ?>" class="raleway easeme title3 <?php echo $class; ?>" data-toggle="collapse" data-parent="#postnested<?php echo $postid2; ?>"><?php the_title(); ?>
																			<?php if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ):
																				echo '<span class="button pull-right right"><i class="fa" aria-hidden="true"></i></span>';	
																			endif; ?>										
																			</a>
																		<?php
																		elseif( !has_child_pages( $post->ID ) ): ?>
																			<a href="<?php the_permalink(); ?>" class="raleway easeme title3 <?php echo $class; ?>">
																				<span class="title"><?php the_title(); ?></span>
																			</a>
																		<?php endif; ?>
																	</div> <!-- end of panel-heading -->
																	<?php
																	if( get_id_using_template('templates/template-mice-calendar-international.php') == $post->ID || get_id_using_template('templates/template-mice-calendar-national.php') == $post->ID ):
																		include(locate_template('partials/tabs-mice-calendar.php'));
																	endif; ?>
																</div> <!-- end of panel-default -->
															<?php $sctr++; endwhile; ?>
														</div> <!-- end of panel-group -->
													</div><!--/.panel-body -->
												</div><!--/.panel-collapse --> 
											<?php elseif( get_id_using_template('templates/template-csr.php') == $post->ID ):
												include(locate_template('partials/tabs-csr.php'));
											endif; ?>
										<?php endwhile; ?>
									<?php endif; ?>
								</div> <!-- end of panel -->
							</div> <!-- end of panel -->
						</div> <!-- end of panel-body -->
					</div> <!-- end of panel -->
				</div> <!-- end of panel -->
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	<?php endif; ?>

<?php endif; ?>

<script type="text/javascript">
	jQuery(window).load(function($){
    	jQuery('a.downbutton').on('click', function(){
    		if( !jQuery(this).hasClass('collapsed') ){
				jQuery(this).prev('a.downbuttonlink').removeClass('activeme2');
    			
    		} else {
				jQuery(this).prev('a.downbuttonlink').addClass('activeme2');
    			
    		}

    	});


	});
</script>
