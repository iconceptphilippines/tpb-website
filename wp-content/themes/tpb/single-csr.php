<?php
ob_start();
get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<?php if( get_field( 'slider' ) ): ?>
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<ul id="csrmainslide">
									<?php foreach( get_field( 'slider' ) as $gal ): ?>
										<li><img src="<?php echo $gal['url']; ?>" alt="<?php echo $gal['title']; ?>" class="img-responsive img-center"></li>
									<?php endforeach; ?>
								</ul>
								<br/>
								<ul id="csrslide">
									<?php $ctr = 0; foreach( get_field( 'slider' ) as $gal ): ?>
										<li data-slideIndex="<?php echo $ctr; ?>"><a href=""><img src="<?php echo $gal['url']; ?>" alt="<?php echo $gal['title']; ?>" class="img-responsive"></a></li>
									<?php $ctr++; endforeach; ?>
								</ul>							
							</div>
						</div>
					<?php endif; ?>
					<div class="table-responsive">
						<br/>
						<br/>
						<br/>
						<?php the_content(); ?>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>