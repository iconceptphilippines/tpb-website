<?php //Template Name: About Us Template ?>
<?php
$queried_object = get_queried_object();
$parent_id = $queried_object->post_parent;
$aboutargs = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'DESC',
    'orderby'        => 'menu_order'
	);
$about = new WP_Query($aboutargs);
if( $about->have_posts() ):
	while( $about->have_posts() ): $about->the_post(); 
		wp_redirect( get_the_permalink() );
	endwhile;
endif; wp_reset_postdata(); ?>

wp_redirect(''); ?>