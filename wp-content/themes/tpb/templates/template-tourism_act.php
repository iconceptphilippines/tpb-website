<?php //Template Name: Tourism Tabs Template ?>
<?php get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); ?>
			</div>
			<div class="col-lg-9">				
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive">
						<?php the_content(); ?>
					</div>				
					<div class="row">
						<div class="t-lists">
							<?php if( get_field( 'tourism_act_lists' ) ):
								$lists = get_field( 'tourism_act_lists' );
								foreach( $lists as $list ): ?>
									<div class="col-lg-4">
										<img src="<?php echo $list['thumbnail']; ?>" alt="<?php echo $list['title']; ?>" class="img-responsive">
										<div class="title raleway"><?php echo $list['title']; ?></div>
										<div class="row">
											<div class="col-lg-6">
												<a href="<?php echo $list['file']; ?>" class="downloadbtn easeme"><i class="fa fa-download" aria-hidden="true"></i>DOWNLOAD</a>
											</div>
											<div class="col-lg-6">
												<a href="<?php echo $list['file']; ?>" class="downloadbtn easeme"><i class="fa fa-eye" aria-hidden="true"></i>VIEW</a>												
											</div>
										</div>
									</div>
								<?php endforeach;
							endif; ?>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>