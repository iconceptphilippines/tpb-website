<?php //Template Name: By Disciple
ob_start();
get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); ?>
			</div>
			<div class="col-lg-9">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive">
						<?php the_content(); ?>
					</div>
					<?php $term_discipline = get_terms( array('discipline'), array( 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
					if( $term_discipline ): ?>
						<div class="term_discipline ralewayimp">
							<?php foreach( $term_discipline as $term ): ?>
								<div class="wrp">
									<div class="title ralewayimp"> <a href="<?php echo get_the_permalink( get_id_using_template( 'templates/template-alpha-listing.php' ) ).'?disciple='.$term->slug; ?>" style="color:#333;"><?php echo $term->name; ?></a></div>
									<div class="description ralewayimp"><?php echo $term->description; ?></div>									
								</div>
							<?php endforeach; ?>
						</div> <!-- end of term_discipline -->
					<?php endif; ?>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>