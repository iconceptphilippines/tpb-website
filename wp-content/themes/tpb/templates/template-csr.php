<?php //Template Name: CSR Template
ob_start();
get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); ?>
			</div>
			<div class="col-lg-9">				
				<?php if(have_posts()): while( have_posts() ): the_post();
					if( isset( $_GET['content'] ) ):
						echo '<div class="table-responsive">';
						the_content();
						echo '</div>';
					else:
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

						if( isset( $_GET['y'] ) ):
							$year = $_GET['y'];
						else:
							$year = '';
						endif;
						if( isset( $_GET['month'] ) ):
							$month = $_GET['month'];
						else:
							$month = '';
						endif;

						$the_query = array(
						    'post_type'  => 'csr',  
							'orderby' => 'date',
							'order'	=> 'ASC',
							'year'		=> $year,
							'monthnum'	=> $month,
							'post_status' => array('publish', 'future'),
							'paged'=>$paged,
							'posts_per_page' => 9 );
						$variable = new WP_Query($the_query);
						if ( $variable->have_posts() ): ?>
							<div class="row">
								<div class="col-sm-12">
									<div class="pagination pull-right">
										<?php echo tpb_pagination2(); ?>									
									</div>
									<div class="clearfix"></div>
								</div>
								<?php while ($variable->have_posts()):
									$variable->the_post();
									if( get_field( 'thumbnail', $post->ID ) ):
										$image = get_field( 'thumbnail', $post->ID );
									else:
										$image = get_bloginfo('template_url')."/images/dummyimg2.png";
									endif;
									?>
									<div class="col-md-4 col-sm-6 col-xs-6 csr-col">
										<div class="csr-cont">
											<div class="img" style="background:url(<?php echo $image; ?>); center center no-repeat; background-size:cover; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-position:center;"></div>
											<div class="title raleway"><?php the_title(); ?></div>
											<div class="excerpt raleway">
												<?php
												$myExcerpt = get_the_excerpt();
												$tags = array("<p>", "</p>");
												$myExcerpt = str_replace($tags, "", $myExcerpt);
												$strcount = strlen($myExcerpt);
												$newexcerpt = substr($myExcerpt, 0,250);
												if ($strcount <= 250):
													echo $myExcerpt;
												else:
													echo $newexcerpt.'...';
												endif;
												?>											
											</div> <!-- end of excerpt -->
											<div class="row">
												<div class="col-lg-6 col-sm-6">
													<p class="time left raleway"><?php the_time('F j, Y') ?></p>
												</div>
												<div class="col-lg-6 col-sm-6">
													<a href="<?php the_permalink(); ?>" class="btn raleway pull-right">READ MORE</a>
												</div>
											</div>
										</div> <!-- end of csr-cont -->
									</div> <!-- end of col-lg-4 -->
								<?php endwhile; ?>
								<div class="col-lg-12 col-xs-12">
									<div class="pagination pull-right"><?php tpb_pagination(); ?></div>
									<div class="clearfix"></div>
								</div>
							</div> <!-- end of row -->
						


					<?php endif;
					endif;
				endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<script type="text/javascript">
	jQuery(window).load(function($){
    	var biggestHeight = 0;

	    jQuery('.csr-cont').each( function(){
		    if( jQuery(this).height() > biggestHeight ){
	    		biggestHeight = jQuery(this).height();
	    	}
	    });
		jQuery('.csr-cont').height( biggestHeight );


	});
</script>

<?php get_footer(); ?>