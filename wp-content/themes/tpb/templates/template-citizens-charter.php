<?php //Template Name: Citizens Charter
ob_start();
get_header(); ?>

<div id="page" class="citizens">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); ?>
			</div>
			<div class="col-lg-9">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive">
						<?php the_content(); ?>
					</div>
					<div class="citizens-cont">
						<div class="row">
							<?php
							if( get_field( 'citizens_charter_lists') ):
								$citizens = get_field( 'citizens_charter_lists');
								foreach( $citizens as $citizen ):
									if( $citizen['image'] ):
										$image = $citizen['image'];
									else:
										$image = get_bloginfo('template_url').'/images/dummyimg1.png';
									endif;?>
									<div class="col-xs-4 citizenbox">
										<div class="citi-image" style="background: url(<?php echo $image; ?>) center 0px no-repeat; background-size:cover; -o-background-size: cover; -moz-background-size: cover; -webkit-background-size: cover; height: 140px; width: 100%; " >		
										</div>
										<div class="citi-title raleway"><?php echo $citizen['title'] ?></div>
										<p class="citi-link"><a download target="_blank" href="<?php echo $citizen['file']; ?>"><i class="fa fa-download " aria-hidden="true"></i>Download</a><a target="_blank" class="raleway" href="<?php echo $citizen['file']; ?>"><i class="fa fa-eye" aria-hidden="true"></i>View</a></p>
									</div>

								<?php endforeach;
							endif; ?>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>