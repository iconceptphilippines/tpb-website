<?php // Template Name: Contact Us Template ?>
<?php get_header(); ?>
	<div id="page">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-sm-6">
					<div id="cf-wrap">
						<?php echo do_shortcode( '[contact-form-7 id="454" title="Contact Us Form"]' ); ?>
					</div>
				</div>
				<div class="col-md-5 col-sm-6">
					<p class="pg-label raleway">Location</p>
					<?php
						if( have_posts( ) ):
							while( have_posts( ) ): the_post( );
								echo get_the_content( );
							endwhile;
						endif;
					?>
				</div>
				<div class="col-md-12 col-sm-12">
					<?php
						if( have_posts( ) ):
							while( have_posts( ) ): the_post( );
								comments_template();
							endwhile;
						endif;
					?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>