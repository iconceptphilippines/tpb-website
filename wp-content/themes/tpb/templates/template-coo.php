<?php //Template Name: COO Template
ob_start();
get_header(); ?>

<div id="page" class="coos">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); ?>
			</div>
			<div class="col-lg-9">
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
					<div class="row">
						<div class="col-lg-9">
							<div class="table-responsive">
								<?php the_content(); ?>
							</div>							
						</div>
						<div class="col-lg-3">
							<?php if( get_field('gallery') ):
								$sctr = 1;
								$cusarray = array();
								$overall = array();

								foreach( get_field('gallery') as $gal ):
									if (($sctr % 2) == 1):
										$cusarray[] = $gal;
										if( count( get_field('gallery') ) == $sctr ):
											$overall[] = $cusarray;
										endif;
									elseif( ($sctr % 2) == 0 ):
										$cusarray[] = $gal;
										$overall[] = $cusarray;
										//clear array
										$cusarray = array();
									endif;
									$sctr++;
								endforeach;
								?>
								<div id="coos-slider">
									<?php foreach( $overall as $gal ):?>
										<div>
											<div class="row nomargin">
												<?php foreach( $gal as $ga ):

												// var_dump(wp_get_attachment_image_src( $ga['id'] ), 'custom-size');
													?>
													<div class="col-lg-6">
														<a href="#lightbox<?php echo $ga['id'] ?>" rel="group1" class="lightbox" data-toggle="tooltip" title="<?php echo $ga['title'] ?>"><img src="<?php echo wp_get_attachment_image_src( $ga['id'], 'thumbnail' )[0]; ?>" alt="<?php $ga['title'] ?>"></a>
														<div id="lightbox<?php echo $ga['id'] ?>" style="display: none;">
															<div class="row">
																<div class="col-md-8 col-md-offset-2">
																	<div class="innerwrapper">
																		<img src="<?php echo $ga['url'] ?>" alt="<?php $ga['title'] ?>" class="img-responsive img-center">
																		<h1 class="title"><?php echo $ga['title']; ?></h1>
																		<div class="desc"><?php echo $ga['description']; ?></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>									
								<div class="controller center">
									  <p><span id="slider-prev"></span>  <span id="slider-next"></span></p>
								</div>
							<?php endif; ?>
						</div>
						<div class="col-lg-12">
							<div class="feeds-wrp">
								<ul class="feeds">
									<?php if( isset( $_GET['open'] ) ):
										if( $_GET['open'] == 'tpb' ):
											$active1 = 'active';
											$active2 = '';
										elseif( $_GET['open'] == 'coo' ):
											$active1 = '';
											$active2 = 'active';
										else:
											$active1 = 'active';
											$active2 = '';
										endif;
									else:
										$active1 = 'active';
										$active2 = '';
									endif; ?>

									<li><a href="<?php echo get_the_permalink().'?open=tpb'; ?>" class="easeme <?php echo $active1; ?>">TPB Social Feeds</a></li>
									<li><a href="<?php echo get_the_permalink().'?open=coo'; ?>" class="easeme <?php echo $active2; ?>">COO's Social Feeds</a></li>
								</ul>
								<div class="row">
									<div id="fb-root"></div>
									<script>(function(d, s, id) {
									  var js, fjs = d.getElementsByTagName(s)[0];
									  if (d.getElementById(id)) return;
									  js = d.createElement(s); js.id = id;
									  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=269150669956903";
									  fjs.parentNode.insertBefore(js, fjs);
									}(document, 'script', 'facebook-jssdk'));</script>
									<?php if( $active1 == 'active' ):
										if( get_field('facebook_link_tpb') ): ?>
											<div class="col-lg-6">
												<div class="fb-page" data-href="<?php echo get_field('facebook_link_tpb'); ?>" data-tabs="timeline" data-width="340" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo get_field('facebook_link_tpb'); ?>" class="fb-xfbml-parse-ignore"><a href="<?php echo get_field('facebook_link_tpb'); ?>">Tourism Promotions Board - formerly PCVC</a></blockquote></div>
											</div>
										<?php endif;
										if( get_field('twitter_link_tpb') ): ?>
											<div class="col-lg-6">
												<a class="twitter-timeline" data-chrome="sampleda" data-width="330" data-height="500" href="<?php echo get_field('twitter_link_tpb') ?>">Tweets</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
											</div>
								        <?php endif;
								       	elseif(  $active2 == 'active'  ):
								       	if( get_field('facebook_link_coos') ): ?>
											<div class="col-lg-6">
												<div class="fb-page" data-href="<?php echo get_field('facebook_link_coos'); ?>" data-tabs="timeline" data-width="340" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo get_field('facebook_link_coos') ?>" class="fb-xfbml-parse-ignore"><a href="<?php echo get_field('facebook_link_coos') ?>">Tourism Promotions Board - formerly PCVC</a></blockquote></div>
											</div>
										<?php endif;
										if( get_field('twitter_link_coos') ): ?>
											<div class="col-lg-6">
												<a class="twitter-timeline" data-width="330" data-height="500" href="<?php echo get_field('twitter_link_coos') ?>">Tweets</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
											</div>
								        <?php endif;
									endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<script type="text/javascript">
	jQuery(window).load(function($){
		jQuery('#coos-slider').bxSlider({
			mode : 'vertical',
			minSlides: 5,
			maxSlides: 5,
			slideWidth: 200,
			slideMargin: 4,
			pager : false,
			// controls : false,
			nextSelector: '#slider-next',
			prevSelector: '#slider-prev',
			nextText: '<i class="fa fa-caret-down" aria-hidden="true"></i>',
			prevText: '<i class="fa fa-caret-up" aria-hidden="true"></i>'
		});
		jQuery(".lightbox").fancybox();
		// jQuery('[data-toggle="tooltip"]').tooltip(); 

	});
</script>

<?php get_footer(); ?>

<!-- 0906-165-6478 -->