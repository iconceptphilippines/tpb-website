<?php //Template Name: Careers Template
ob_start();
get_header(); ?>

<div id="page" class="careers">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive">
						<?php the_content(); ?>
					</div>
					<div class="table-responsive">
						<div class="note">
							<?php if( get_field('note') ):
								echo get_field('note');
							endif;?>							
						</div>
					</div>
					<div class="careers-slider-cont">
						<?php
						if( isset( $_GET['open'] ) ):
							if( $_GET['open'] == 'positions' ):
								$active1 = 'active';
								$active2 = '';
								$open = 'positions';
							elseif( $_GET['open'] == 'opportunities' ):
								$active1 = '';
								$active2 = 'active';
								$open = 'opportunities';

							else:	
								$active1 = 'active';
								$active2 = '';
								$open = 'positions';								
							endif;	
						else:
							$active1 = 'active';
							$active2 = '';
							$open = 'positions';								

						endif;

						if( get_field( 'vacant_positions' ) ): $careers = get_field( 'vacant_positions' ); 
						?>
							<a href="<?php echo get_the_permalink().'?open=positions'; ?>" class="sliderlink easeme raleway <?php echo $active1; ?>">TPB VACANT POSITIONS <span class="pull-right"><i class="fa" aria-hidden="true"></i></span></a>

						<?php endif;
						if( $open == 'positions' ): ?>
							<div id="careerslider">
								<ul class="resp-tabs-list vert raleway">
									<?php $ctr = 1; foreach( $careers as $career ):  ?>
										<li>  <?php echo $career['title']; ?>  </li>
									<?php $ctr++; endforeach; ?>
								</ul>
								<div class="resp-tabs-container vert raleway"> 
									<?php $ctr = 1; foreach( $careers as $career ):  ?>
										<div> <div class="table-responsive"><?php echo $career['description']; ?></div> </div>
									<?php $ctr++; endforeach; ?>
								</div>
							</div>
							<div class="clearfix"></div>
						<?php endif;
						if( get_field( 'opportunities' ) ): $opps = get_field( 'opportunities', get_the_id() ); ?>

							<a href="<?php echo get_the_permalink().'?open=opportunities'; ?>" class="sliderlink easeme raleway <?php echo $active2; ?>">TPB OPPORTUNITIES <span class="pull-right"><i class="fa" aria-hidden="true"></i></span></a>

						<?php
						endif;
						if( $open == 'opportunities' ): ?>
							<div id="careerslider">
								<ul class="resp-tabs-list vert raleway">
									<?php $ctr = 1; foreach( $opps as $opportunity ):  ?>
										<li>  <?php echo $opportunity['title']; ?>  </li>
									<?php $ctr++; endforeach; ?>
								</ul>
								<div class="resp-tabs-container vert raleway"> 
									<?php $ctr = 1; foreach( $opps as $opportunity ):  ?>
										<div> <div class="table-responsive"><?php echo $opportunity['description']; ?></div> </div>
									<?php $ctr++; endforeach; ?>
								</div>
							</div>
							<div class="clearfix"></div>							

						<?php endif; ?>

					</div> <!-- end of careers-slider-cont -->
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>