<?php //Template Name: Gender and Development
ob_start();
get_header(); ?>

<div id="page" class="gender">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); ?>
			</div>
			<div class="col-lg-9">
				<?php if(have_posts()): while( have_posts() ): the_post();
					if( isset( $_GET['content'] ) ):
						$contentid = $_GET['content'];
					else:
						$contentid = 0;
					endif;
					if( get_field('contents') ): $contents = get_field('contents');
						$ctr = 1; ?>
						<label for="content" class="raleway">Select Content</label>
						<select name="content" class="form-control">
							<option value="<?php the_permalink(); ?>">-Select-</option>
							<?php foreach( $contents as $content ):
								if( $contentid == $ctr ):
									$selected = 'selected';
								else:
									$selected = '';
								endif; ?>
								<option value="<?php echo get_the_permalink().'?content='.$ctr; ?>" <?php echo $selected; ?> ><?php echo $content['title'] ?></option>
							<?php $ctr++; endforeach; ?>
						</select>
					<?php endif; ?>

					<div class="table-responsive">					
						<?php
						if( $contentid == 0 ):
							the_content();
						else:
							if( get_field('contents') ): $contents = get_field('contents'); $ctr = 1; 
								foreach( $contents as $content ):
									if( $contentid == $ctr ):
										echo $content['content'];
										break;
									endif;
								$ctr++; endforeach;
							endif;
						endif;
						?>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<script type="text/javascript">
	jQuery(window).load(function($){
	    jQuery("select[name='content']").on( 'change', function(e) {
	    	e.preventDefault();
		   var val = jQuery("select[name='content']").val();
	       window.location = val;
	    });
	});
</script>


<?php get_footer(); ?>