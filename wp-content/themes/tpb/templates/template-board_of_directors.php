<?php //Template Name: Board Director Tabs Template ?>
<?php get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); wp_reset_postdata(); ?>
			</div>
			<div class="col-lg-9">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive" style="margin-bottom: 15px; ">
						<?php the_content(); ?>
					</div>
					<div class="row">
						<?php 
						if( get_field( 'board_of_directors', get_the_id() ) ):
							$boards = get_field( 'board_of_directors', get_the_id() );
							foreach( $boards as $board ):
								if( $board['image'] ):
									$image = $board['image'];
								else:
									$image = get_bloginfo('template_url').'/images/dummyimg1.png'; 
								endif; ?>
								<div class="col-md-4 col-xs-6 b-col">
									<div class="b-wrapper">
										<div class="b-img" style="background: url(<?php echo $image; ?>) center 0px no-repeat; background-size:cover; -o-background-size: cover; -moz-background-size: cover; -webkit-background-size: cover; ">
										</div>
										<div class="b-desc table-responsive">											
											<?php echo $board['description']; ?>
										</div> <!-- end of b-desc -->
										<?php if( $board['resume'] ): ?>
											<div class="center"> <a href="<?php echo $board['resume']; ?>">Click Here For Resume</a>	</div>	
										<?php endif; ?>	
									</div> <!-- end of b-wrapper -->
								</div>
						<?php endforeach;
						endif; ?>
						<div class="col-lg-12">
							<div class="rd-hr">
								<div class="left raleway rd-title">REPRESENTATIVE DIRECTORS</div>
							</div>
						</div>
						<?php 
						if( get_field( 'representative_directors', get_the_id() ) ):
							$boards = get_field( 'representative_directors', get_the_id() );
							foreach( $boards as $board ):
								if( $board['image'] ):
									$image = $board['image'];
								else:
									$image = get_bloginfo('template_url').'/images/dummyimg1.png'; 
								endif; ?>
								<div class="col-md-4 col-xs-6 b-col">
									<div class="b-wrapper">
										<div class="b-img" style="background: url(<?php echo $image; ?>) center 0px no-repeat; background-size:cover; -o-background-size: cover; -moz-background-size: cover; -webkit-background-size: cover; ">
										</div>
										<div class="b-desc table-responsive">											
											<?php echo $board['description']; ?>
										</div> <!-- end of b-desc -->
										<?php if( $board['resume'] ): ?>
											<div class="center"> <a href="<?php echo $board['resume']; ?>">Click Here For Resume</a>	</div>
										<?php endif; ?>
									</div> <!-- end of b-wrapper -->
								</div>
						<?php endforeach;
						endif; ?>
						<div class="col-lg-12">
							<div class="rd-hr"></div>
						</div>
						<?php 
						if( get_field( 'other_directors', get_the_id() ) ):
							$boards = get_field( 'other_directors', get_the_id() );
							foreach( $boards as $board ):
								if( $board['image'] ):
									$image = $board['image'];
								else:
									$image = get_bloginfo('template_url').'/images/dummyimg1.png'; 
								endif; ?>
								<div class="col-md-4 col-xs-6 b-col">
									<div class="b-wrapper">
										<div class="b-img" style="background: url(<?php echo $image; ?>) center 0px no-repeat; background-size:cover; -o-background-size: cover; -moz-background-size: cover; -webkit-background-size: cover; ">
										</div>
										<div class="b-desc table-responsive">											
											<?php echo $board['description']; ?>
										</div> <!-- end of b-desc -->
										<?php if( $board['resume'] ): ?>
											<div class="center"> <a href="<?php echo $board['resume']; ?>">Click Here For Resume</a>	</div>
										<?php endif; ?>	
									</div> <!-- end of b-wrapper -->
								</div>
						<?php endforeach;
						endif; ?>
					</div> <!-- end of row -->					
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<script type="text/javascript">
	jQuery(window).load(function($){
    	var biggestHeight = 0;

	    jQuery('.b-wrapper').each( function(){
		    if( jQuery(this).height() > biggestHeight ){
	    		biggestHeight = jQuery(this).height();
	    	}
	    });
		jQuery('.b-wrapper').height( biggestHeight );


	});
</script>

<?php get_footer(); ?>