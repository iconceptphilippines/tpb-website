<?php // Template Name: Brochure Template ?>
<?php get_header('extended'); ?>
	<div id="page">
		<div class="container">
			<div class="row">
				<?php
					if( have_rows( 'brochure_list' ) ):
						while( have_rows( 'brochure_list' ) ): the_row( );
							$display_img 	= get_sub_field( 'brochure_display_image' ) ? get_sub_field( 'brochure_display_image' ) : '' ;
							$new_url 		= $display_img ? $display_img['url'] : 'http://placehold.it/263x140';
							$file 			= get_sub_field( 'brochure_file' );
							// var_dump($file);
							?>
							<div class="col-sm-4 col-md-3">
								<div class="img-wrap-cover" style="background-image:url('<?php echo $new_url; ?>');"></div>
								<p class="brochure-title raleway"><?php echo get_sub_field( 'brochure_title' ); ?></p>
								<p class="brochure-link"><a download target="_blank" href="<?php echo $file['url']; ?>"><i class="fa fa-download" aria-hidden="true"></i>Download</a><a target="_blank" href="<?php echo $file['url']; ?>"><i class="fa fa-eye" aria-hidden="true"></i>View</a></p>
							</div>
							<?php
						endwhile;
					endif;
				?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>