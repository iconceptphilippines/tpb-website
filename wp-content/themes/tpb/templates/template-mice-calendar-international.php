<?php //Template Name: M.I.C.E International ?>
<?php get_header(); ?>

<div id="page" class="mice-calendar">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); wp_reset_postdata(); ?>
			</div>
			<div class="col-lg-9">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive">
						<?php the_content(); ?>
					</div>
					<?php include(locate_template('partials/mice-filter.php')); ?>

					<div class="row">
						<?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

						if( $post_id_name ):
							$args = array(
								'post_type' => 'mice-international',
								'p'	=> $post_id_name,
								'post_status' => array('publish', 'future'),
								'posts_per_page' => 1,
							);

						else:
							$args = array(
								'post_type' => 'mice-international',
								'orderby'	=> $orderby,
								'order'		=> 'ASC',
								'year'		=> $year,
								'monthnum'	=> $month,
								'post_status' => array('publish', 'future'),
								'posts_per_page' => 4,
								'paged'=>$paged
							);
						endif;
						$variable = new WP_Query($args);
						if ($variable->have_posts()): ?>
							<div class="col-sm-12">
								<div class="pagination pull-right">
									<?php echo tpb_pagination(); ?>									
								</div>
								<div class="clearfix"></div>
							</div>
							<?php while( $variable->have_posts() ): $variable->the_post(); ?>
								<div class="col-lg-12">
									<div class="mice-cal-title raleway"><?php the_title(); ?></div>
									<div class="mice-cal-date raleway"> Date:  <?php if( get_field('date') ): echo get_field('date'); endif; ?></div>
									<div class="mice-cal-details raleway">
										<b class="raleway">EVENT DETAILS</b>
										<table>
											<tbody>
												<tr>
													<td>Venue:   </td>
													<td><?php if( get_field('venue') ): echo get_field('venue'); endif; ?></td>
												</tr>
												<tr>
													<td>Host:   </td>
													<td><?php if( get_field('host') ): echo get_field('host'); endif; ?></td>
												</tr>
												<tr>
													<td>Expected Attendance:   </td>
													<td><?php if( get_field('expected_attendance') ): echo get_field('expected_attendance'); endif; ?></td>
												</tr>
												<tr>
													<td>Additional Details</td>
													<td><?php if( get_field('additional_information') ): echo get_field('additional_information'); endif; ?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							<?php endwhile ?>
						<?php else: echo 'no posts'; endif; ?>
						<div class="col-sm-12">
							<div class="pagination pull-right">
								<?php echo tpb_pagination2(); ?>
							</div>
							<div class="clearfix"></div>
						</div>
					</div> <!-- end of row -->
				<?php endwhile; endif; ?>
			</div> <!-- end of col-lg-9 -->
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>