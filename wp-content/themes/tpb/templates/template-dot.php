<?php //Template Name: DOT
ob_start();
get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive">
						<?php the_content(); ?>
					</div>
					<div class="agencies">
						<div class="row">
							<?php if( get_field('agencies') ): $agencies = get_field('agencies');
								foreach( $agencies as $agent ):
									if( $agent['link'] ):
										$url = $agent['link'];
									else:
										$url = site_url();
									endif; ?>
								<div class="col-md-6 col-sm-12">
									<div class="agencies-wrp">
										<div class="row">
											<div class="col-lg-5">
												<a href="<?php echo $url; ?>" target='_blank'>
													<div class="img-wrp">
														<div class="img-tble">
															<img src="<?php echo $agent['image']; ?>" alt="img" class="img-responsive img-center">
														</div>	
													</div>
												</a>
											</div>
											<div class="col-lg-7">
												<div class="details table-responsive">
													<?php echo $agent['description']; ?>
												</div>
											</div>
										</div>
									</div> <!-- end of agencies-wrp -->
								</div>
								<?php endforeach;
							endif; ?>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<script type="text/javascript">
	jQuery(window).load(function($){
    	var biggestHeight = 0;

	    jQuery('.agencies-wrp').each( function(){
		    if( jQuery(this).height() > biggestHeight ){
	    		biggestHeight = jQuery(this).height();
	    	}
	    });
		jQuery('.agencies-wrp').height( biggestHeight );


	});
</script>

<?php get_footer(); ?>