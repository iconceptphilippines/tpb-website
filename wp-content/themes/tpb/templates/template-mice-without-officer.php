<?php //Template Name: M.I.C.E (Sort by Month) ?>
<?php get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); wp_reset_postdata(); ?>
			</div>
			<div class="col-lg-9">
				<?php
				global $wpdb, $post;

				$link = get_the_permalink( $post->ID );
				$thistype = get_field('type');

				$month_filter = $wpdb->get_col('SELECT DISTINCT(mt1.meta_value) 
				               FROM '.$wpdb->posts.' AS p1 
				               INNER JOIN '.$wpdb->postmeta.' AS mt1 ON p1.ID = mt1.post_id
				               INNER JOIN '.$wpdb->postmeta.' AS mt2 ON p1.ID = mt2.post_id
				               INNER JOIN '.$wpdb->term_relationships.' AS t1 ON p1.ID = t1.object_id
				   
				               AND mt1.meta_key = "month_filter"
				               AND t1.term_taxonomy_id = '.$thistype.'
				               AND p1.post_type = "events"
				               AND p1.post_status = "publish"
				               WHERE mt1.meta_value != ""
				               ORDER BY mt1.meta_value ASC');
				sort($month_filter);
				if( $month_filter ):
					foreach( $month_filter as $month ):
						$the_query = array(
						    'post_type'  => 'events', 
							'tax_query' => array(
								array(
									'taxonomy' => 'type',
					                'field' => 'id',
					                'terms' => $thistype,
					                'operator' => 'IN'
								),
							'monthnum'	=> $month,
							),
							'posts_per_page' => -1 );
						$variable = new WP_Query($the_query);
						if ( $variable->have_posts() ): $postids = array();
							while ($variable->have_posts()): $variable->the_post();
								if( !in_array(  get_the_id(), $postids ) ):
									$postids[] = get_the_id();
								endif;
							endwhile;
						endif;
						sort( $postids );
					endforeach; ?>
					<div class="mice-cont">
						<div class="filters">

							<div class="row">
								<div class="col-lg-4">
									<p class="raleway">Filter by Month:</p>
									<select name="country" id="country" class="form-control raleway" onChange="document.location = this.value">
										<option value="">-Select-</option>
										<?php foreach( $month_filter as $month ):
											$dateObj   = DateTime::createFromFormat('!m', $month);
											$monthName = $dateObj->format('F');
											echo '<option value="'.$link.'?mnth='.$month.'">'.ucfirst($monthName ).'</option>';
										endforeach; ?>
									</select>
								</div>
								<div class="col-lg-4">
									<p class="raleway">Filter by Event:</p>
									<select name="postevent" id="postevent" class="form-control raleway" onChange="document.location = this.value">
										<option value="">-Select-</option>
										<?php foreach( $postids as $postid ):
											echo '<option value="'.$link.'?postevent='.$postid.'">'.get_the_title($postid).'</option>';
										endforeach; ?>
									</select>
								</div>
							</div>
						</div> <!-- end of filters -->

						<div class="mic-wrp" id="mic-wrp">
							<table class="table table-responsive" id="eventlists">
								<thead>
									<tr>
										<th width="25%">NAME OF EVENT</th>
										<th width="25%">DATE/VENUE</th>
										<th width="50%">BRIEF DESCRIPTION</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$type = 0;
									if( isset( $_GET['postevent'] ) ):
										$type = 1;
										$query = array(
										'page_id' => $_GET['postevent'],
									    'post_type'  => 'events',
										'tax_query' => array(
											array(
												'taxonomy' => 'type',
								                'field' => 'id',
								                'terms' => $thistype,
								                'operator' => 'IN'
											),
										),
										'order'	=> 'DESC',
										'orderby'	=> 'date',
										'posts_per_page' => -1 );
									elseif( isset( $_GET['mnth'] ) ):
										$type = 2;
										$query = array(
										    'post_type'  => 'events', 
											'tax_query' => array(
												array(
													'taxonomy' => 'type',
									                'field' => 'id',
									                'terms' => $thistype,
									                'operator' => 'IN'
												),
											),
											'meta_query' => array(
												'relation' => 'AND',
												array(
													'key' => 'month_filter',
													'value'	=> $_GET['mnth'],
													'compare' => '=',
													'type' => 'CHAR',  
												),
											),
											'orderby'	=> 'menu_order',
											'order'		=> ASC,
											'posts_per_page' => -1 );
									else:
										$type = 3;
									endif;
									if( $type == 1 ):
										$var = new WP_Query($query);
										if ( $var->have_posts() ): $month_ctr = 1;
											while ($var->have_posts()): $var->the_post();
												if( $month_ctr == 1 ):
													$dateObj   = DateTime::createFromFormat('!m', get_field('month_filter') );
													$monthName = $dateObj->format('F');													
													?>
													<tr class='e-country'>
														<td colspan="1" class="raleway"><img src="<?php echo get_bloginfo('template_url').'/images/mini_title_icon.png' ?>" alt="icon"><?php echo $monthName; ?> - <?php if( get_field( get_field( 'month_filter' ).'_description', 'type_'.$thistype ) ):
																echo get_field( get_field( 'month_filter' ).'_description', 'type_'.$thistype );
															endif; ?></td>
														<td colspan="3">
															<?php
															if( get_field( get_field( 'month_filter' ).'_event', 'type_'.$thistype ) ):
																echo get_field( get_field( 'month_filter' ).'_event', 'type_'.$thistype );
															endif;
															?>
														</td>
													</tr>
												<?php endif; $month_ctr++; ?>
												<tr>
													<?php if( get_field( 'single_page_content' ) ): ?>
														<td width="25%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
													<?php else: ?>
														<td width="25%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><?php the_title(); ?></td>
													<?php endif; ?>			
													<td width="25%" class="e-venue-date raleway">
														<?php if( get_field('date') ): echo get_field('date'); endif; ?>
														<?php if( get_field('venue') ): echo get_field('venue'); endif; ?>					
													</td>
													<td width="50%" class="e-desc raleway"><?php the_content(); ?></td>
												</tr>
											<?php endwhile;
										endif;
										wp_reset_postdata();  ?>										

									<?php elseif( $type == 2 ):
										$var = new WP_Query($query);
										$dateObj   = DateTime::createFromFormat('!m', $_GET['mnth'] );
										$monthName = $dateObj->format('F');
										?>
										<tr class='e-country'>
											<td colspan="1" class="raleway"><img src="<?php echo get_bloginfo('template_url').'/images/mini_title_icon.png' ?>" alt="icon"><?php echo $monthName; ?> - <?php if( get_field( $_GET['mnth'].'_description', 'type_'.$thistype ) ):
													echo get_field( $_GET['mnth'].'_description', 'type_'.$thistype );
												endif; ?></td>
											<td colspan="3">
												<?php
												if( get_field( $_GET['mnth'].'_event', 'type_'.$thistype ) ):
													echo get_field( $_GET['mnth'].'_event', 'type_'.$thistype );
												endif;
												?>
											</td>
										</tr>
										<?php if ( $var->have_posts() ):
											while ($var->have_posts()): $var->the_post(); ?>
												<tr>
													<?php if( get_field( 'single_page_content' ) ): ?>
														<td width="25%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
													<?php else: ?>
														<td width="25%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><?php the_title(); ?></td>
													<?php endif; ?>			
													<td width="25%" class="e-venue-date raleway">
														<?php if( get_field('date') ): echo get_field('date'); endif; ?>
														<?php if( get_field('venue') ): echo get_field('venue'); endif; ?>					
													</td>
													<td width="50%" class="e-desc raleway"><?php the_content(); ?></td>
												</tr>
											<?php endwhile;
										endif;
										wp_reset_postdata();  ?>
									<?php elseif( $type == 3 ):
										foreach( $month_filter as $month ):
											$dateObj   = DateTime::createFromFormat('!m', $month);
											$monthName = $dateObj->format('F');
											$query = array(
											    'post_type'  => 'events', 
												'tax_query' => array(
													array(
														'taxonomy' => 'type',
										                'field' => 'id',
										                'terms' => $thistype,
										                'operator' => 'IN'
													),
												),
												'meta_query' => array(
													'relation' => 'AND',
													array(
														'key' => 'month_filter',
														'value'	=> $month,
														'compare' => '=',
														'type' => 'CHAR',  
													),
												),
												'orderby'	=> 'menu_order',
												'order'		=> ASC,
												'posts_per_page' => -1 );
											$var = new WP_Query($query);
											?>
											<tr class='e-country'>
												<td colspan="1" class="raleway"><img src="<?php echo get_bloginfo('template_url').'/images/mini_title_icon.png' ?>" alt="icon"><?php echo $monthName; ?> - <?php if( get_field( $month.'_description', 'type_'.$thistype ) ):
														echo get_field( $month.'_description', 'type_'.$thistype );
													endif; ?></td>
												<td colspan="3">
													<?php
													if( get_field( $month.'_event', 'type_'.$thistype ) ):
														echo get_field( $month.'_event', 'type_'.$thistype );
													endif;
													?>
												</td>
											</tr>
											<?php if ( $var->have_posts() ):
												while ($var->have_posts()): $var->the_post(); ?>
													<tr>
														<?php if( get_field( 'single_page_content' ) ): ?>
															<td width="25%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
														<?php else: ?>
															<td width="25%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><?php the_title(); ?></td>
														<?php endif; ?>			
														<td width="25%" class="e-venue-date raleway">
															<?php if( get_field('date') ): echo get_field('date'); endif; ?>
															<?php if( get_field('venue') ): echo get_field('venue'); endif; ?>					
														</td>
														<td width="50%" class="e-desc raleway"><?php the_content(); ?></td>
													</tr>
												<?php endwhile;
											endif;
											wp_reset_postdata();  ?>
										<?php endforeach; ?>

									<?php endif; ?>
								</tbody> <!-- end of table -->
							</table> <!-- end of table-responsive -->
							<div id="editor"></div>
							<div class="pdfbutton">
								<?php if( isset( $_GET['mnth'] ) ):
									$mpdflink = get_bloginfo('template_url').'/partials/htmltopdf-by-month.php?type='.$thistype.'&mnth='.$_GET['mnth'];
								elseif( isset( $_GET['postevent'] ) ):
									$mpdflink = get_bloginfo('template_url').'/partials/htmltopdf-by-month.php?type='.$thistype.'&postevent='.$_GET['postevent'];
								else:
									$mpdflink = get_bloginfo('template_url').'/partials/htmltopdf-by-month.php?type='.$thistype;
								endif;
								?>

								<form action="<?php echo $mpdflink; ?>" method='POST' target="_blank" id="formbutton">
									<a href="" type="submit" class="pdfbuttonid link"> <img src="<?php echo get_bloginfo('template_url').'/images/pdflogo.png'; ?>" alt="image" class="img-responsive"></a>
									<a href="" class="pdfbuttonid">Download to PDF</a>
								</form>
							</div>
						</div> <!-- end of mic-wrp -->
					</div> <!-- end of mice-conts -->

				<?php endif; ?>
			</div> <!-- end of col-lg-9 -->
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>