<?php
//Template Name: Page Not Found
get_header(); ?>

<div id="si-page">
	<?php if(have_posts()): while(have_posts()): the_post();
		$content = get_the_content();
			echo '<div class="container" style="padding:30px 15px;">'; ?>
				<div class="row">
					<div class="table-ronsponsive">
						<?php the_content(); ?>
					</div> <!-- end of table-responsive -->
				</div> <!-- end of row -->
			</div> <!-- end of container -->
		<?php endwhile;
	endif; wp_reset_postdata(); ?>
</div> <!-- end of si-page -->



<?php get_footer(); ?>