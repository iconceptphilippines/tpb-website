<?php //Template Name: Alpha Listing
ob_start();
get_header(); ?>

<div id="page" class="alpha">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); ?>
			</div>
			<div class="col-lg-9">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<?php $pageurl = get_the_permalink(); ?>
					<div class="table-responsive">
						<?php the_content(); ?>
					</div>
					<h3 class="raleway">ALPHABETICAL LISTING OF ASSOCIATIONS</h3>
					<h3 class="raleway">WITH CORRESPONDING DISCIPLINE</h3>

					<div class="alpa-form">
						<h4 class="raleway">Search Association by</h4>
					</div> <!-- end of alpa-form -->
					<div class="row">
						<div class="col-md-6">
							<?php
							$term_discipline = get_terms( array('discipline'), array( 'hide_empty' => true, 'orderby' => 'name', 'order' => 'ASC' ) );
							if( $term_discipline ): ?>
								<select name="discipline" id="" class="form-control">
									<option value="?php echo get_the_permalink().'?disciple=all'; ?>">All Discipline</option>
									<?php foreach( $term_discipline as $term_disc ):
										if( isset( $_GET['disciple'] ) ):
											if( $_GET['disciple'] == $term_disc->slug ):
												$selected = 'selected';
											else:
												$selected = '';
											endif;
										else:
											$selected = '';
										endif;
									?>
										<option value="<?php echo get_the_permalink().'?disciple='.$term_disc->slug; ?>" <?php echo $selected; ?> ><?php echo $term_disc->name ?></option>
									<?php endforeach;  ?>
								</select>
							<?php endif; ?>

							
						</div>
						<div class="col-md-2 nopadding">
							<a href="#" class="btn easeme btndiscipline form-control raleway" >VIEW</a>
						</div>
					</div>
					<table class="table">
						<thead>
							<tr>
								<th><b>NO</b></th>
								<th><b>ASSOCIATION NAME</b></th>
								<th><b>DISCIPLINE</b></td>
							</tr>
						</thead>`
						<tbody>
							<?php 
							if( isset( $_GET['disciple'] ) ):
								if( $_GET['disciple'] != 'all' || $_GET['disciple'] != '' ):
									$termslug = array( $_GET['disciple'] );
									$terms =array(
										array(
											'taxonomy' => 'discipline',
							                'field' => 'slug',
							                'terms' => $termslug,
							                'operator' => 'IN'
										),
									);
								else:
									$terms = array();
								endif;
							else:
								$terms = array();
							endif;
							$args = array(
								'post_type' => 'alpha-listing',
								'orderby'	=> 'name',
								'order'		=> 'ASC',
								'tax_query' => $terms,
								'posts_per_page' => -1
							);
							$variable = new WP_Query($args);
							if ($variable->have_posts()): $ctr = 1;
								while( $variable->have_posts() ): $variable->the_post();?>
									<tr>
										<td><?php echo $ctr; ?></td>
										<td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
										<td>										
											<?php
											$disciplines = get_the_terms( get_the_ID(), 'discipline' );
											if( $disciplines ):
												foreach( $disciplines as $discipline ): ?>
													<a href="<?php echo $pageurl.'?disciple='.$discipline->slug; ?>">
														<?php echo $discipline->name; ?>
													</a>
												<?php endforeach;
											endif;
											?>
										</td>
									</tr>
								<?php $ctr++; endwhile; 
							else:
								echo '<tr><td>No post</td></tr>';
							endif;
							wp_reset_postdata(); ?>
						</tbody> <!-- end of tbody -->
					</table>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<script type="text/javascript">
	jQuery(window).load(function($){
	    jQuery('a.btndiscipline').on( 'click', function(e) {
	    	e.preventDefault();
		   var val = jQuery("select[name='discipline']").val();
	       window.location = val;
	    });
	});
</script>

<?php get_footer(); ?>