<?php //Template Name: Tabs Template ?>
<?php
ob_start();
get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); ?>
			</div>
			<div class="col-lg-9">
				<div class="table-responsive">
					<?php if(have_posts()): while( have_posts() ): the_post();
						the_content();
					endwhile; endif; ?>
				</div>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>