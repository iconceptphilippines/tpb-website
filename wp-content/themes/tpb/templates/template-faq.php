<?php //Template Name: FAQ Template
ob_start();
get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php if(have_posts()): while( have_posts() ): the_post(); ?>
					<div class="table-responsive">
						<?php the_content(); ?>
					</div>
					<?php
					if( get_field( 'qa' ) ): $qas = get_field( 'qa' ); ?>
						<div id="qaslider">
							<ul class="resp-tabs-list vert raleway">
								<?php foreach( $qas as $qa ): ?>
									<li><?php echo $qa['question'] ?></li>
								<?php endforeach; ?>
							</ul>
							<div class="resp-tabs-container vert raleway"> 
								<?php foreach( $qas as $qa ): ?>
									<div>
										<div class="table-responsive content">
											<?php echo $qa['answer']; ?>
										</div>
									</div>
								<?php endforeach; ?>								
							</div>
						</div> <!-- end of qaslider -->
					<?php endif; ?>
				<?php endwhile; endif; ?>
			</div>
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>