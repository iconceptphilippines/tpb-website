<?php //Template Name: M.I.C.E ?>
<?php get_header(); ?>

<div id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php get_sidebar('tabs'); wp_reset_postdata(); ?>
			</div>
			<div class="col-lg-9">
				<?php
				global $post;
				$link = get_the_permalink( $post->ID );
				$thistype = get_field('type');
				$the_query = array(
				    'post_type'  => 'events', 
					'tax_query' => array(
						array(
							'taxonomy' => 'type',
			                'field' => 'id',
			                'terms' => $thistype,
			                'operator' => 'IN'
						),
					),
					'posts_per_page' => -1 );
				$variable = new WP_Query($the_query);
				if ( $variable->have_posts() ): $country_array = array(); $postids = array(); $postdates = array(); ?>
					<div class="mice-cont">
						<?php while ($variable->have_posts()): $variable->the_post();
							if( !in_array(  get_field( 'country' ), $country_array ) ):
								$country_array[] = get_field( 'country' );
							endif;
							if( !in_array(  get_the_id(), $postids ) ):
								$postids[] = get_the_id();
							endif;
							if( !in_array(  get_field( 'date' ), $postdates ) ):
								$postdates[] = get_field( 'date' );
							endif;
						endwhile; wp_reset_postdata();
						sort( $country_array );
						sort( $postids );
						sort( $postdates );


						if( $country_array ): ?>
							<div class="filters">
								<div class="row">
									<div class="col-lg-4">
										<p class="raleway">Filter by Country:</p>
										<select name="country" id="country" class="form-control raleway" onChange="document.location = this.value">
											<option value="">-Select-</option>
											<?php foreach( $country_array as $country ):
												echo '<option value="'.$link.'?country='.$country.'">'.ucfirst($country ).'</option>';
											endforeach; ?>
										</select>
									</div>
									<div class="col-lg-4">
										<p class="raleway">Filter by Date:</p>
										<select name="postdate" id="postdate" class="form-control raleway" onChange="document.location = this.value">
											<option value="">-Select-</option>
											<option value="<?php echo $link.'?postdate=desc'; ?>">Latest to Oldest</option>
											<option value="<?php echo $link.'?postdate=asc'; ?>">Oldest to Latest</option>

										</select>
									</div>
									<div class="col-lg-4">
										<p class="raleway">Filter by Event:</p>
										<select name="postevent" id="postevent" class="form-control raleway" onChange="document.location = this.value">
											<option value="">-Select-</option>
											<?php foreach( $postids as $postid ):
												echo '<option value="'.$link.'?postevent='.$postid.'">'.get_the_title($postid).'</option>';
											endforeach; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="mic-wrp" id="mic-wrp">
								<?php $type = 0;
								if( isset( $_GET['country'] ) ):
									$type = 1;
									$the_query2 = array(
									    'post_type'  => 'events',  
										'order'	=> 'ASC',
										'orderby'	=> 'meta_value',
										'meta_query' => array(
											'relation' => 'AND',
											array(
												'key'	=> 'country',
												'value'=> $_GET['country'],
												'type' => 'CHAR',
												'compare' => '=', 
											),
										),
										'tax_query' => array(
											array(
												'taxonomy' => 'type',
								                'field' => 'id',
								                'terms' => $thistype,
								                'operator' => 'IN'
											),
										),
										'posts_per_page' => -1 );
								elseif( isset( $_GET['postdate'] ) ):
									$type = 2;
									$the_query2 = array(
									    'post_type'  => 'events',  
										'tax_query' => array(
											array(
												'taxonomy' => 'type',
								                'field' => 'id',
								                'terms' => $thistype,
								                'operator' => 'IN'
											),
										),
										'order'	=> $_GET['postdate'],
										'orderby'	=> 'date',
										'posts_per_page' => -1 );
								elseif( isset( $_GET['postevent'] ) ):
									$type = 2;
									$the_query2 = array(
										'page_id' => $_GET['postevent'],
									    'post_type'  => 'events',
										'tax_query' => array(
											array(
												'taxonomy' => 'type',
								                'field' => 'id',
								                'terms' => $thistype,
								                'operator' => 'IN'
											),
										),
										'order'	=> 'DESC',
										'orderby'	=> 'date',
										'posts_per_page' => -1 );
								endif; ?>
								<table class="table table-responsive" id="eventlists">
									<thead>
										<tr>
											<th width="20%">NAME OF EVENT</th>
											<th width="15%">DATE/VENUE</th>
											<th width="40%">BRIEF DESCRIPTION</th>
											<th width="25%">PROJECT OFFICER</th>
										</tr>
									</thead>
									<tbody>
										<?php if( $type == 1 ): ?>
											<tr class='e-country'><td colspan="4" class="raleway"><img src="<?php echo get_bloginfo('template_url').'/images/mini_title_icon.png' ?>" alt="icon"><?php $name = str_replace('_', ' ', $_GET['country'] ); echo strtoupper( $name ); ?></td></tr>
											<?php
											$variable2 = new WP_Query($the_query2);
											if ( $variable2->have_posts() ):				
												while ($variable2->have_posts()): $variable2->the_post(); ?>
													<tr>
														<?php if( get_field( 'single_page_content' ) ): ?>
															<td width="20%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
														<?php else: ?>
															<td width="20%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><?php the_title(); ?></td>
														<?php endif; ?>			
														<td width="15%" class="e-venue-date raleway">
															<?php if( get_field('date') ): echo get_field('date'); endif; ?>
															<?php if( get_field('venue') ): echo get_field('venue'); endif; ?>					
														</td>
														<td width="40%" class="e-desc raleway"><?php the_content() ?></td>
														<td width="25%" class="e-proj-off raleway"><?php if( get_field('project_officer') ): echo get_field('project_officer'); endif; ?></td>
													</tr>
												<?php endwhile;
											else:
												echo '<tr><td colspan="4" class="raleway center">No result found.</td></tr>';
											endif; wp_reset_postdata();?>
										<?php elseif( $type == 2 ):
											$variable2 = new WP_Query($the_query2);
											if ( $variable2->have_posts() ):				
												while ($variable2->have_posts()): $variable2->the_post(); ?>
													<tr class='e-country'><td colspan="4" class="raleway"><img src="<?php echo get_bloginfo('template_url').'/images/mini_title_icon.png' ?>" alt="icon"><?php $name = str_replace('_', ' ', get_field('country') ); echo strtoupper( $name ); ?></td></tr>
													<tr>
														<?php if( get_field( 'single_page_content' ) ): ?>
															<td width="20%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
														<?php else: ?>
															<td width="20%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><?php the_title(); ?></td>
														<?php endif; ?>			
														<td width="15%" class="e-venue-date raleway">
															<?php if( get_field('date') ): echo get_field('date'); endif; ?>
															<?php if( get_field('venue') ): echo get_field('venue'); endif; ?>					
														</td>
														<td width="40%" class="e-desc raleway"><?php the_content() ?></td>
														<td width="25%" class="e-proj-off raleway"><?php if( get_field('project_officer') ): echo get_field('project_officer'); endif; ?></td>
													</tr>
												<?php endwhile;
											else:
												echo '<tr><td colspan="4" class="raleway center">No result found.</td></tr>';
											endif; wp_reset_postdata();?>
										<?php else:
											foreach( $country_array as $country ): ?>
												<tr class='e-country'><td colspan="4" class="raleway"><img src="<?php echo get_bloginfo('template_url').'/images/mini_title_icon.png' ?>" alt="icon"><?php $name = str_replace('_', ' ', $country ); echo strtoupper( $name ); ?></td></tr>
												<?php
												$the_query2 = array(
												    'post_type'  => 'events',  
													'order'	=> 'ASC',
													'orderby'	=> 'meta_value',
													'meta_query' => array(
														'relation' => 'AND',
														array(
															'key'	=> 'country',
															'value'=> $country,
															'type' => 'CHAR',
															'compare' => '=', 
														),
													),
													'tax_query' => array(
														array(
															'taxonomy' => 'type',
											                'field' => 'id',
											                'terms' => $thistype,
											                'operator' => 'IN'
														),
													),
													'posts_per_page' => -1 );
												$variable2 = new WP_Query($the_query2);
												if ( $variable2->have_posts() ):				
													while ($variable2->have_posts()): $variable2->the_post(); ?>
														<tr>
															<?php if( get_field( 'single_page_content' ) ): ?>
																<td width="20%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
															<?php else: ?>
																<td width="20%" class="e-name raleway" id="<?php echo get_the_id(); ?>"><?php the_title(); ?></td>
															<?php endif; ?>			
															<td width="15%" class="e-venue-date raleway">
																<?php if( get_field('date') ): echo get_field('date'); endif; ?>
																<?php if( get_field('venue') ): echo get_field('venue'); endif; ?>					
															</td>
															<td width="40%" class="e-desc raleway"><?php the_content() ?></td>
															<td width="25%" class="e-proj-off raleway"><?php if( get_field('project_officer') ): echo get_field('project_officer'); endif; ?></td>
														</tr>
													<?php endwhile;
												else:
													echo '<tr><td colspan="4" class="raleway center">No result found.</td></tr>';
												endif; wp_reset_postdata();?>
											<?php endforeach; ?>
										<?php endif; ?>										
									</tbody>
								</table> <!-- end of table -->
							</div> 
							<div id="editor"></div>
							<div class="pdfbutton">
								<?php if( isset( $_GET['country'] ) ):
									$mpdflink = get_bloginfo('template_url').'/partials/htmltopdf.php?type='.$thistype.'&country='.$_GET['country'];
								elseif( isset( $_GET['postdate'] ) ):
									$mpdflink = get_bloginfo('template_url').'/partials/htmltopdf.php?type='.$thistype.'&postdate='.$_GET['postdate'];
								elseif( isset( $_GET['postevent'] ) ):
									$mpdflink = get_bloginfo('template_url').'/partials/htmltopdf.php?type='.$thistype.'&postevent='.$_GET['postevent'];
								else:
									$mpdflink = get_bloginfo('template_url').'/partials/htmltopdf.php?type='.$thistype;
								endif;
								?>
								<form action="<?php echo $mpdflink; ?>" method='POST' target="_blank" id="formbutton">
									<a href="" type="submit" class="pdfbuttonid link"> <img src="<?php echo get_bloginfo('template_url').'/images/pdflogo.png'; ?>" alt="image" class="img-responsive"></a>
									<a href="" class="pdfbuttonid">Download to PDF</a>
								</form>				
							</div>
						<?php endif; ?>
					</div> <!-- end of mice-cont -->			
				<?php endif; ?>
			</div> <!-- end of col-lg-9 -->
		</div> <!-- end of row -->
	</div> <!-- end of container -->
</div> <!-- end of page -->

<?php get_footer(); ?>