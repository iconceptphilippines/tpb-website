<div id="<?php echo get_post_type() ?>-archive" class="panel-collapse sidebar-archive">
	<div class="panel-body">
		<div class="panel-group" id="sidebar-<?php echo get_post_type(); ?>">
			<?php
			$years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".get_post_type()."' ORDER BY post_date DESC");
			if( $years ):
				foreach( $years as $year ):
					if( isset( $postyear ) ):
						if(  $postyear  == $year ):
							$classyear = 'activeyear';
							$classcollapse = 'in';
						else:
							$classyear = '';
							$classcollapse = '';							
						endif;
					else:
						$classyear = '';
						$classcollapse = '';
					endif;
					?>
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" class="raleway easeme year <?php echo $classyear; ?>" data-parent="#sidebar-<?php echo get_post_type(); ?>" href="#<?php echo get_post_type().'-'.$year ?>">
								<?php echo $year; ?>
							</a>
						</div><!--/.panel-heading -->
						<div id="<?php echo get_post_type().'-'.$year ?>" class="panel-collapse collapse <?php echo $classcollapse; ?>">
							<div class="panel-body">
								<?php $months = $wpdb->get_col("SELECT DISTINCT MONTH(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".get_post_type()."' AND YEAR(post_date) = '".$year."' ORDER BY post_date ASC");
								foreach( $months as $month ):
									$monthletter = date( 'F', mktime(0, 0, 0, $month,1 ) ); 

									if( isset( $postmonth ) ):
										if( $postyear == $year && $postmonth == $month ):
											$classmonth = 'activemonth';
										else:
											$classmonth = '';
										endif;
									else:
										$classmonth = '';
									endif;

									?>
									<a class="raleway easeme month <?php echo $classmonth; ?>" href="<?php echo get_post_type_archive_link('whats-new').'/'.$year.'/'.sprintf("%02d", $month) ?>/">
										<?php echo $monthletter; ?>
									</a>
								<?php endforeach; ?>
							</div><!--/.panel-body -->
						</div><!--/.panel-collapse --> 
					</div><!-- /.panel --> 
					
				<?php endforeach;
			endif; ?>
		</div> <!-- end of panel-group -->
	</div> <!-- end of panel-body -->
</div> <!-- end of panel-collapse -->

