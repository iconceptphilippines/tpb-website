<?php

if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">
	

	<div class="row">
		<div class="col-lg-12">
			<?php

				if( is_user_logged_in() ):
					$class = "comment-form-comment";
				else:
					$class = "comment-form-comment2";
				endif;
				$aria_req = ( $req ? " aria-required='true'" : '' );
				$fields =  array(

				  'author' =>
				    '<p class="comment-form-author"><input id="author" name="author" class="form-control easeme raleway" placeholder="Name*" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
				    '" size="30"' . $aria_req . ' /></p>',

				  'email' =>
				    '<p class="comment-form-email"><input id="email" name="email" class="form-control easeme raleway" placeholder="Email Address*" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
				    '" size="30"' . $aria_req . ' /></p>',

				  'url' =>
				    '<p class="comment-form-url"><input id="url" name="url" class="form-control easeme raleway" placeholder="Website" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
				    '" size="30" /></p>',
				);

				comment_form( array(
					'title_reply_before' => '<h2 id="reply-title" class="comment-reply-title montserrat">',
					'title_reply_after'  => '</h2>',
					'title_reply'		 => '<span class="raleway">Leave a Feedback</div>',
					'logged_in_as'		 => '',
					'comment_field'		 => '<p class="'.$class.' pull-left"><textarea rows="4" placeholder="Leave a feedback" class="form-control raleway" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea><p><div class="clearfix"></div>',
					'label_submit'		 => 'SUBMIT',
					'title_reply_to'	 => '<i class="fa fa-comments" aria-hidden="true"></i> Leave a Reply to %s',
					'class_submit'		 => 'btn glbl-btn1 easeme',
					'fields' => apply_filters( 'comment_form_default_fields', $fields ),

				) );
			?>			
		</div>
	</div>


</div><!-- .comments-area -->
