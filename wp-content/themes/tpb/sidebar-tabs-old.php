<div class="tabs">
	<?php
	$currentid = get_the_id();

	$ancestor = get_post_ancestors($currentid);
	$depth = count( $ancestor );

	$parent0 = false;
	$parent1 = false;
	$parent2 = false;
	$parent3 = false;

	if( $depth == 0 || $depth == 1 ):
		if( $post->post_parent ):
			$parent0 = $post->post_parent ;
		else:
			$parent0 = get_the_id();
		endif;

		$childargs = array(
		    'post_type'      => 'page',
		    'posts_per_page' => -1,
		    'post_parent'    => $parent0,
		    'order'          => 'ASC',
		    'orderby'        => 'menu_order'
			);
		$child = new WP_Query($childargs);
		if( $child->have_posts() ): ?>
			<ul>
				<?php while( $child->have_posts() ): $child->the_post();
					if( get_the_id() == $currentid ):
						$class = 'active';
					else:	
						$class = '';
					endif;
					?>
					<li class="<?php echo $class; ?> easeme">
						<a href="<?php the_permalink(); ?>" class="raleway easeme"><?php the_title(); ?></a>
						<?php
						$childargs2 = array(
						    'post_type'      => 'page',
						    'posts_per_page' => -1,
						    'post_parent'    => get_the_ID(),
						    'order'          => 'ASC',
						    'orderby'        => 'menu_order'
							);
						$child2 = new WP_Query($childargs2);
						if( $child2->have_posts() ): ?>
							<ul class="subchild">
								<?php while( $child2->have_posts() ): $child2->the_post(); ?>
									<li class="<?php echo $class; ?> easeme"><a href="<?php the_permalink(); ?>" class="raleway easeme"><?php the_title(); ?></a></li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; wp_reset_postdata(); ?>
	<?php elseif( $depth == 2 ):
		$childargs = array(
		    'post_type'      => 'page',
		    'posts_per_page' => -1,
		    'post_parent'    => $ancestor[1],
		    'order'          => 'ASC',
		    'orderby'        => 'menu_order'
			);
		$child = new WP_Query($childargs);
		if( $child->have_posts() ): ?>
			<ul>
				<?php while( $child->have_posts() ): $child->the_post();
					if( $post->ID == $ancestor[0] ):
						$class = 'active';
					else:	
						$class = '';
					endif;
					?>
					<li class="<?php echo $class; ?> easeme"><a href="<?php the_permalink(); ?>" class="raleway easeme"><?php the_title(); ?></a>
						<?php $childargs2 = array(
						    'post_type'      => 'page',
						    'posts_per_page' => -1,
						    'post_parent'    => get_the_ID(),
						    'order'          => 'ASC',
						    'orderby'        => 'menu_order'
							);
						$child2 = new WP_Query($childargs2);
						if( $child2->have_posts() ): ?>
							<ul class="subchild">
								<?php while( $child2->have_posts() ): $child2->the_post();
									if( $post->ID == $currentid ):
										$class = 'active2';
									else:	
										$class = '';
									endif;								
									?>
									<li class="<?php echo $class; ?> easeme"><a href="<?php the_permalink(); ?>" class="raleway easeme"><?php the_title(); ?></a></li>
								<?php endwhile; ?>								
							<ul>
						<?php endif; ?>
					</li>
				<?php endwhile; wp_reset_postdata();  ?>
			</ul>
		<?php endif; wp_reset_postdata(); ?>

	<?php endif; ?>
</div>